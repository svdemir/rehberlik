﻿namespace rbsProje.Kontroller.Uyeler
{
    partial class UCogrenciIslemleri
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.uCogrenciEkle1 = new rbsProje.Kontroller.Uyeler.UCogrenciEkle();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.uCuyeTopluDuzenle1 = new rbsProje.Kontroller.Uyeler.UCuyeTopluDuzenle();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.uCuyeTopluEkle1 = new rbsProje.Kontroller.Uyeler.UCuyeTopluEkle();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(812, 603);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Silver;
            this.tabPage3.Controls.Add(this.uCogrenciEkle1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(804, 577);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Öğrenci Ekle";
            // 
            // uCogrenciEkle1
            // 
            this.uCogrenciEkle1.Location = new System.Drawing.Point(39, 4);
            this.uCogrenciEkle1.Name = "uCogrenciEkle1";
            this.uCogrenciEkle1.Size = new System.Drawing.Size(670, 568);
            this.uCogrenciEkle1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Silver;
            this.tabPage4.Controls.Add(this.uCuyeTopluDuzenle1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(804, 586);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Toplu Öğrenci Düzenle";
            // 
            // uCuyeTopluDuzenle1
            // 
            this.uCuyeTopluDuzenle1.Location = new System.Drawing.Point(7, 40);
            this.uCuyeTopluDuzenle1.Name = "uCuyeTopluDuzenle1";
            this.uCuyeTopluDuzenle1.Size = new System.Drawing.Size(801, 527);
            this.uCuyeTopluDuzenle1.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.Silver;
            this.tabPage5.Controls.Add(this.uCuyeTopluEkle1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(804, 586);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Toplu Öğrenci Ekle";
            // 
            // uCuyeTopluEkle1
            // 
            this.uCuyeTopluEkle1.Location = new System.Drawing.Point(34, 29);
            this.uCuyeTopluEkle1.Name = "uCuyeTopluEkle1";
            this.uCuyeTopluEkle1.Size = new System.Drawing.Size(725, 560);
            this.uCuyeTopluEkle1.TabIndex = 0;
            // 
            // UCogrenciIslemleri
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "UCogrenciIslemleri";
            this.Size = new System.Drawing.Size(815, 607);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private UCuyeTopluDuzenle uCuyeTopluDuzenle1;
        private System.Windows.Forms.TabPage tabPage5;
        private UCogrenciEkle uCogrenciEkle1;
        private UCuyeTopluEkle uCuyeTopluEkle1;
    }
}
