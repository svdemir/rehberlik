﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Data.SQLite;
using DGVPrinterHelper;

namespace rbsProje.Kontroller.Uyeler
{
    public partial class UCogrenciIstatistik : UserControl
    {
        public UCogrenciIstatistik()
        {
            CheckForIllegalCrossThreadCalls = false;

            InitializeComponent();
        }

        string aralikBaslangic;
        string aralikBitis;
        object gorusmeYapan;

        private void btnGorIstGetir_Click(object sender, EventArgs e)
        {
            gorusmeYapan = comboBox1.SelectedValue;

            if (radioButton3.Checked)                   // tüm zamanlar
            {
                aralikBaslangic = "00000000";
                aralikBitis = "99991230";
            }
            if (radioButton1.Checked)                   // son bir ay
            {
                aralikBaslangic = DateTime.Today.AddMonths(-1).ToString("yyyyMMdd");
                aralikBitis = DateTime.Today.ToString("yyyyMMdd");
            }
            if (radioButton2.Checked)
            {
                aralikBaslangic = (DateTime.Today.Year - 1).ToString() + "0915";
                aralikBitis = DateTime.Today.ToString("yyyyMMdd");
            }
            if (radioButton4.Checked)
            {
                aralikBaslangic = dateTimePicker1.Value.ToString("yyyyMMdd");
                aralikBitis = dateTimePicker2.Value.ToString("yyyyMMdd");
            }


            if (radioOgrenci.Checked)
            {
                OgrenciBazindaSiralama();
                label4.Text = "*Görüşmelerin detaylarını görmek için.\n             Sağ taraftan seçim yapınız.";
            }
            else
            {
                SinifBazindaSiralama();
                label4.Text = "*Görüşmelerin detaylarını görmek için.\n           Sağ taraftan seçim yapınız.";
            }

            GenelIstatistik();

            dataGridView1.AllowUserToAddRows = false;
            btnIstYazdır.Enabled = true;
        }

        private void OgrenciBazindaSiralama()
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT g.ogrNo,u.AD,u.SOYAD,count(ogrNO) AS GORUSMELER  FROM gorusmeOgrenci g INNER JOIN UYELER u ON g.ogrNO = u.[NO] WHERE g.calismayiYapan=@3 and g.gorusmeTarihi BETWEEN @1 AND @2 GROUP BY u.[NO] HAVING count(* ) > 0 ORDER BY count(g.ogrNo) DESC");

            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi
            komut.Parameters.AddWithValue("@3", gorusmeYapan);

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Görüşme Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView1.Columns[0].HeaderText = "Öğrenci No";
            dataGridView1.Columns[1].HeaderText = "Adı";
            dataGridView1.Columns[2].HeaderText = "Soyadı";
            dataGridView1.Columns[3].HeaderText = "Görüşme Sayısı";


            dataGridView1.Columns[0].Width = 55;
            dataGridView1.Columns[1].Width = 120;
            dataGridView1.Columns[2].Width = 100;
            dataGridView1.Columns[3].Width = 65;

            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void OgrenciGorusmeDetayGetir(string ogrNo)
        {
            SorunAlanlariAnaliz(ogrNo);
            YapilacakCalismaAnaliz(ogrNo);
        }
        
        private void SorunAlanlariAnaliz(string ogrNo)
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT g.ogrNo,
                                                           k.sorunAlani,
                                                           count(k.sorunAlani) AS deger
                                                      FROM gorusmeOgrenci g
                                                           INNER JOIN
                                                           analizSorunAlanlari a ON g.gorusmeID = a.gorusmeID
                                                           INNER JOIN
                                                           kaynakSorunAlanlari k ON a.sorunAlani = k.alanID
                                                     WHERE g.calismayiYapan = @3 AND 
                                                           g.ogrNO = @4 AND 
                                                           g.gorusmeTarihi BETWEEN @1 AND @2
                                                     GROUP BY k.sorunAlani
                                                     ORDER BY deger DESC");

            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi
            komut.Parameters.AddWithValue("@3", gorusmeYapan);
            komut.Parameters.AddWithValue("@4", Convert.ToInt64(ogrNo));

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                gridSorunAlan.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Görüşme Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            gridSorunAlan.Columns[0].Visible = false;


            gridSorunAlan.Columns[1].HeaderText = "Sorun Alanı";
            gridSorunAlan.Columns[2].HeaderText = "Sayısı";


            gridSorunAlan.Columns[1].Width = 150;
            gridSorunAlan.Columns[2].Width = 75;

            gridSorunAlan.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridSorunAlan.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridSorunAlan.AllowUserToAddRows = false;

        }

        private void YapilacakCalismaAnaliz(string ogrNo)
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT g.ogrNo,
                                                           k.yapilacakCalisma,
                                                           count(k.yapilacakCalisma) AS deger
                                                      FROM gorusmeOgrenci g
                                                           INNER JOIN
                                                           analizYapilacakCalismalar a ON g.gorusmeID = a.gorusmeID
                                                           INNER JOIN
                                                           kaynakYapilacakCalismalar k ON a.yapilacakCalisma = k.yapilacakID
                                                     WHERE g.calismayiYapan = @3 AND 
                                                           g.ogrNo = @4 AND 
                                                           g.gorusmeTarihi BETWEEN @1 AND @2
                                                     GROUP BY k.yapilacakCalisma
                                                     ORDER BY deger DESC");

            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi
            komut.Parameters.AddWithValue("@3", gorusmeYapan);
            komut.Parameters.AddWithValue("@4", Convert.ToInt64(ogrNo));

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                gridYapilacaCalismaAnaliz.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Görüşme Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            gridYapilacaCalismaAnaliz.Columns[0].Visible = false;


            gridYapilacaCalismaAnaliz.Columns[1].HeaderText = "PDR Alanı";
            gridYapilacaCalismaAnaliz.Columns[2].HeaderText = "Sayısı";


            gridYapilacaCalismaAnaliz.Columns[1].Width = 150;
            gridYapilacaCalismaAnaliz.Columns[2].Width = 75;

            gridYapilacaCalismaAnaliz.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridYapilacaCalismaAnaliz.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridYapilacaCalismaAnaliz.AllowUserToAddRows = false;

        }

        private void SinifBazindaSiralama()
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT u.SINIF,count(u.SINIF) AS GORUSMELER FROM gorusmeOgrenci g INNER JOIN UYELER u ON g.ogrNO = u.[NO] WHERE g.calismayiYapan=@3 and g.gorusmeTarihi BETWEEN @1 AND @2 GROUP BY u.SINIF HAVING count( * ) > 0 ORDER BY count(u.SINIF) DESC");


            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi
            komut.Parameters.AddWithValue("@3", gorusmeYapan);

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                dataGridView1.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Görüşme Kayıt Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            dataGridView1.Columns[0].HeaderText = "Sınıf";
            dataGridView1.Columns[1].HeaderText = "Görüşme Sayısı";

            dataGridView1.Columns[0].Width = 120;
            dataGridView1.Columns[1].Width = 120;
            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void SinifGorusmeDetayGetir(string sinif)
        {
            SinifSorunAlanlariAnaliz(sinif);
            SinifYapilacakAnaliz(sinif);
        }

        private void SinifSorunAlanlariAnaliz(string sinif)
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT u.SINIF,
                                                           k.sorunAlani,
                                                           count(k.sorunAlani) AS deger
                                                      FROM gorusmeOgrenci g
                                                           INNER JOIN
                                                           UYELER u ON u.[NO] = g.ogrNo
                                                           INNER JOIN
                                                           analizSorunAlanlari a ON g.gorusmeID = a.gorusmeID
                                                           INNER JOIN
                                                           kaynakSorunAlanlari k ON a.sorunAlani = k.alanID
                                                     WHERE g.calismayiYapan = @3 AND 
                                                           u.SINIF = @4 AND 
                                                           g.gorusmeTarihi BETWEEN @1 AND @2
                                                     GROUP BY k.sorunAlani
                                                     ORDER BY deger DESC");

            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi
            komut.Parameters.AddWithValue("@3", gorusmeYapan);
            komut.Parameters.AddWithValue("@4", sinif);

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                gridSorunAlan.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Görüşme Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            gridSorunAlan.Columns[0].Visible = false;


            gridSorunAlan.Columns[1].HeaderText = "Sorun Alanı";
            gridSorunAlan.Columns[2].HeaderText = "Sayısı";


            gridSorunAlan.Columns[1].Width = 150;
            gridSorunAlan.Columns[2].Width = 75;

            gridSorunAlan.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridSorunAlan.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridSorunAlan.AllowUserToAddRows = false;

        }

        private void SinifYapilacakAnaliz(string sinif)
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT u.SINIF,
                                                           k.yapilacakCalisma,
                                                           count(k.yapilacakCalisma) AS deger
                                                      FROM gorusmeOgrenci g
                                                           INNER JOIN
                                                           UYELER u ON u.[NO] = g.ogrNo
                                                           INNER JOIN
                                                           analizYapilacakCalismalar a ON g.gorusmeID = a.gorusmeID
                                                           INNER JOIN
                                                           kaynakYapilacakCalismalar k ON a.yapilacakCalisma = k.yapilacakID
                                                     WHERE g.calismayiYapan = @3 AND 
                                                           u.SINIF = @4 AND 
                                                           g.gorusmeTarihi BETWEEN @1 AND @2
                                                     GROUP BY k.yapilacakCalisma
                                                     ORDER BY deger DESC");

            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi
            komut.Parameters.AddWithValue("@3", gorusmeYapan);
            komut.Parameters.AddWithValue("@4", sinif);

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                gridYapilacaCalismaAnaliz.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Görüşme Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            gridYapilacaCalismaAnaliz.Columns[0].Visible = false;


            gridYapilacaCalismaAnaliz.Columns[1].HeaderText = "PDR Alanı";
            gridYapilacaCalismaAnaliz.Columns[2].HeaderText = "Sayısı";


            gridYapilacaCalismaAnaliz.Columns[1].Width = 150;
            gridYapilacaCalismaAnaliz.Columns[2].Width = 75;

            gridYapilacaCalismaAnaliz.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridYapilacaCalismaAnaliz.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridYapilacaCalismaAnaliz.AllowUserToAddRows = false;

        }
        
        private void GenelIstatistik()
        {
            GorusmeSayisiGetir();
            GorusmeTuruSiralama();
            GorusmePDRSiralama();
        }

        private void GorusmeSayisiGetir()
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT count( * ) 
                                                              FROM gorusmeOgrenci g
                                                             WHERE g.calismayiYapan = @3 AND 
                                                                   g.gorusmeTarihi BETWEEN @1 AND @2");


            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi
            komut.Parameters.AddWithValue("@3", gorusmeYapan);

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                string sayim = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                lblSonucSayi.Text = sayim;
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Görüşme Kayıt Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void GorusmeTuruSiralama()
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT g.gorusmeTuru,
                                                               k.gorusmeTuru AS GorusmeAdı,
                                                               count(g.gorusmeTuru) AS GorusmeSayısı
                                                          FROM gorusmeOgrenci g
                                                               INNER JOIN
                                                               kaynakGorusmeTuru k ON g.gorusmeTuru = k.gorusmeTurID
                                                         WHERE g.calismayiYapan = @3 AND 
                                                               g.gorusmeTarihi BETWEEN @1 AND @2
                                                         GROUP BY g.gorusmeTuru
                                                        HAVING count( * ) > 0
                                                         ORDER BY count(g.gorusmeTuru) DESC");


            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi
            komut.Parameters.AddWithValue("@3", gorusmeYapan);

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                gridGorusmeTuru.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Öğrenci Görüşme Sayıları", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            gridGorusmeTuru.Columns[0].Visible = false;

            gridGorusmeTuru.Columns[1].HeaderText = "Görüşme Adı";
            gridGorusmeTuru.Columns[2].HeaderText = "Görüşme Sayısı";

            gridGorusmeTuru.Columns[1].Width = 120;
            gridGorusmeTuru.Columns[2].Width = 120;
            gridGorusmeTuru.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridGorusmeTuru.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridGorusmeTuru.AllowUserToAddRows = false;
        }

        private void GorusmePDRSiralama()
        {
            SQLiteCommand komut = new SQLiteCommand(@"SELECT g.pdrAlani,
                                                           k.pdrAlani AS GorusmeAdı,
                                                           count(g.pdrAlani) AS GorusmeSayısı
                                                      FROM gorusmeOgrenci g
                                                           INNER JOIN
                                                           kaynakPdrAlanlari k ON g.pdrAlani = k.alanID
                                                     WHERE g.calismayiYapan = @3 AND 
                                                           g.gorusmeTarihi BETWEEN @1 AND @2
                                                     GROUP BY g.pdrAlani
                                                    HAVING count( * ) > 0
                                                     ORDER BY count(g.pdrAlani) DESC");


            komut.Parameters.AddWithValue("@1", aralikBaslangic);          //  başlangıç tarihi
            komut.Parameters.AddWithValue("@2", aralikBitis);          //  bitiş  tarihi
            komut.Parameters.AddWithValue("@3", gorusmeYapan);

            try
            {
                DataSet ds = Temel_Metodlar.SqlKomutCalistirSorgu(komut);
                gridPdrAlan.DataSource = ds.Tables[0];
                komut.Dispose();
                ds.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi.", "Öğrenci Görüşme Sayıları", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            gridPdrAlan.Columns[0].Visible = false;

            gridPdrAlan.Columns[1].HeaderText = "PDR Alanı";
            gridPdrAlan.Columns[2].HeaderText = "Görüşme Sayısı";

            gridPdrAlan.Columns[1].Width = 120;
            gridPdrAlan.Columns[2].Width = 120;
            gridPdrAlan.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridPdrAlan.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            gridPdrAlan.AllowUserToAddRows = false;
        }
                
        void radioButonGridTemizle()
        {
            dataGridView1.DataSource = null;
            gridGorusmeTuru.DataSource = null;
            gridPdrAlan.DataSource = null;
            gridSorunAlan.DataSource = null;
            gridYapilacaCalismaAnaliz.DataSource = null;
            lblSonucSayi.Text = "";
            btnIstYazdır.Enabled = false;
        }

        private void radioOgrenci_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }

        private void radioSinif_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();

        }
        
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
            if (radioButton4.Checked)
            {
                groupBox3.Enabled = true;
            }
            else
            {
                groupBox3.Enabled = false;
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (radioOgrenci.Checked)
            {
                OgrenciGorusmeDetayGetir(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            }
            else
            {
                SinifGorusmeDetayGetir(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            }
            groupBoxDetayGridleri.Visible = true;
        }

        private void UCogrenciIstatistik_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                comboBox1.DataSource = veritabanim.rehberOgretmenler.OrderBy(p => p.rehberOgretmenAdSoyad).ToList();
                comboBox1.DisplayMember = "rehberOgretmenAdSoyad";
                comboBox1.ValueMember = "rehberID";
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnGorIstGetir.Enabled = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            radioButonGridTemizle();
        }

        private void btnIstYazdır_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Yazdırma işleminde ilk başta yazıcıyı seçip, önizleme yaparak yazdır butonuna basınız.", "Yazdırma İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            DGVPrinter printer = new DGVPrinter();
            printer.Title = "Öğrenci Görüşme Sayıları Listesi";
            printer.PageNumbers = true;
            printer.PageNumberInHeader = true;
            printer.ShowTotalPageNumber = true;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;
            printer.CellAlignment = StringAlignment.Center;
            printer.TableAlignment = DGVPrinter.Alignment.Center;

            string okulAdi;
            try
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var sorgu = veritabanim.ayarlar.Where(a => a.ID == 1).SingleOrDefault();
                    okulAdi = sorgu.OKULADI;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Okul adı girilmemiş, Ayarlar Kısmından ekleyebilirsiniz.", "Kitap Okuyanlar Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string altBaslik;
            if (aralikBaslangic == "00000000" & aralikBitis == "99991230")
            {
                aralikBaslangic = "";
                aralikBitis = "";
                altBaslik = "";
            }
            else
            {
                aralikBaslangic = aralikBaslangic.Insert(4, "-").Insert(7, "-");
                aralikBitis = aralikBitis.Insert(4, "-").Insert(7, "-");
                altBaslik = aralikBaslangic + " / " + aralikBitis + " Tarihleri Arasında";
            }

            printer.SubTitle = altBaslik;
            printer.Footer = okulAdi + " - " + DateTime.Now.ToLongDateString();
            printer.FooterAlignment = StringAlignment.Center;
            printer.PrintPreviewZoom = 0.7;
            printer.PrintPreviewDataGridView(dataGridView1);
        }
    }
}
