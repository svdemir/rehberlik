﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace rbsProje.Kontroller.Uyeler
{
    public partial class UCuyeGorusme2 : Uyeler.UCuyeListele2
    {
        public UCuyeGorusme2()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void DBkaynakVerileriGetir()
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                cmbGorusmeTuru.DataSource = veritabanim.kaynakGorusmeTuru.OrderBy(g => g.gorusmeTuru).ToList();
                cmbGorusmeTuru.DisplayMember = "gorusmeTuru";
                cmbGorusmeTuru.ValueMember = "gorusmeTurID";

                cmbPdrAlanlari.DataSource = veritabanim.kaynakPdrAlanlari.OrderBy(p => p.pdrAlani).ToList();
                cmbPdrAlanlari.DisplayMember = "pdrAlani";
                cmbPdrAlanlari.ValueMember = "alanID";

                cmbGorusmeyiYapan.DataSource = veritabanim.rehberOgretmenler.OrderBy(p => p.rehberOgretmenAdSoyad).ToList();
                cmbGorusmeyiYapan.DisplayMember = "rehberOgretmenAdSoyad";
                cmbGorusmeyiYapan.ValueMember = "rehberID";


                clistSorunAlanlari.DataSource = veritabanim.kaynakSorunAlanlari.OrderBy(s => s.sorunAlani).ToList();
                clistSorunAlanlari.DisplayMember = "sorunAlani";
                clistSorunAlanlari.ValueMember = "alanID";

                clistYapilacakCalismalar.DataSource = veritabanim.kaynakYapilacakCalismalar.OrderBy(y => y.yapilacakCalisma).ToList();
                clistYapilacakCalismalar.DisplayMember = "yapilacakCalisma";
                clistYapilacakCalismalar.ValueMember = "yapilacakID";

                clistGorusmeSonuc.DataSource = veritabanim.kaynakSonucCumleleri.OrderBy(s => s.sonucCumlesi).ToList();
                clistGorusmeSonuc.DisplayMember = "sonucCumlesi";
                clistGorusmeSonuc.ValueMember = "cumleID";
            }
        }

        private void btnGorusmeKaydet_Click(object sender, EventArgs e)
        {
            GorusmeKaydetme(null);
            KontrolTemizleme();
        }

        long genelGorusmeID;

        public void GorusmeKaydetme(string pramGorusmeID)
        {

            if (string.IsNullOrEmpty(pramGorusmeID))
            {
                genelGorusmeID = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
            }
            else
            {
                genelGorusmeID = Convert.ToInt64(pramGorusmeID);
            }

            

            long ogrNoNumeric = Convert.ToInt64(txtNo.Text);

            using (dbEntities veritabanim = new dbEntities())
            {
                try
                {
                    gorusmeOgrenci ogrenciGorusmem = new gorusmeOgrenci();
                    ogrenciGorusmem.gorusmeID = genelGorusmeID;
                    ogrenciGorusmem.calismayiYapan = Convert.ToInt64(cmbGorusmeyiYapan.SelectedValue);

                    analizGorusmeKonulari konum = new analizGorusmeKonulari();
                    konum.gorusmeID = genelGorusmeID;
                    konum.ogrNo = ogrNoNumeric;
                    konum.gorusmeKonusu = richTextBox1.Text;
                    veritabanim.analizGorusmeKonulari.Add(konum);

                    for (int i = 0; i < clistGorusmeSonuc.CheckedItems.Count; i++)
                    {
                        analizSonucCumleleri asc = new analizSonucCumleleri();
                        asc.gorusmeID = genelGorusmeID;
                        asc.ogrNo = ogrNoNumeric;
                        asc.sonucCumlesi = ((kaynakSonucCumleleri)clistGorusmeSonuc.CheckedItems[i]).cumleID.ToString();
                        veritabanim.analizSonucCumleleri.Add(asc);
                    }

                    ogrenciGorusmem.gorusmeTarihi = dateTimePicker1.Value.ToString("yyyyMMdd");
                    ogrenciGorusmem.gorusmeTuru = Convert.ToInt64(cmbGorusmeTuru.SelectedValue);
                    ogrenciGorusmem.ogrNo = ogrNoNumeric;
                    ogrenciGorusmem.pdrAlani = Convert.ToInt64(cmbPdrAlanlari.SelectedValue);

                    for (int i = 0; i < clistSorunAlanlari.CheckedItems.Count; i++)
                    {
                        analizSorunAlanlari sorunAlanim = new analizSorunAlanlari();
                        sorunAlanim.gorusmeID = genelGorusmeID;
                        sorunAlanim.ogrNo = ogrNoNumeric;
                        sorunAlanim.sorunAlani = ((kaynakSorunAlanlari)clistSorunAlanlari.CheckedItems[i]).alanID.ToString();
                        veritabanim.analizSorunAlanlari.Add(sorunAlanim);
                    }

                    for (int i = 0; i < clistYapilacakCalismalar.CheckedItems.Count; i++)
                    {
                        analizYapilacakCalismalar islemlerim = new analizYapilacakCalismalar();
                        islemlerim.gorusmeID = genelGorusmeID;
                        islemlerim.ogrNo = ogrNoNumeric;
                        islemlerim.yapilacakCalisma = ((kaynakYapilacakCalismalar)clistYapilacakCalismalar.CheckedItems[i]).yapilacakID.ToString();
                        veritabanim.analizYapilacakCalismalar.Add(islemlerim);
                    }
                    veritabanim.gorusmeOgrenci.Add(ogrenciGorusmem);
                    veritabanim.SaveChanges();
                    MessageBox.Show("Görüşme kayıt işlemi başarılı.", "Görüşme Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("Hata. Alanlara dikkat ediniz.", "Görüşme Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void btnOgrenciGorusmeYazdir_Click(object sender, EventArgs e)
        {
            PrintDocument doc = new PrintDocument();
            doc.PrintPage += new PrintPageEventHandler(doc_PrintPage);
            PrintPreviewDialog pdg = new PrintPreviewDialog();
            pdg.Document = doc;
            pdg.Height = 700;
            pdg.Width = 900;
            pdg.Show();

        }

        private void doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.Black, 3), new Rectangle(60, 80, 700, 1030));  //Genel Çerçeve
            e.Graphics.DrawString("GÖRÜŞME / PSİKOLOJİK DANIŞMA RAPORU", new System.Drawing.Font(new FontFamily("Segoe UI"), 16, FontStyle.Bold), Brushes.Black, new RectangleF(160, 100, 620, 365));

            string okulAdim;
            using (dbEntities veritabanim = new dbEntities())
            {
                okulAdim = veritabanim.ayarlar.FirstOrDefault().OKULADI;
            }

            StringFormat format = new StringFormat();
            format.LineAlignment = StringAlignment.Center;
            format.Alignment = StringAlignment.Center;
            e.Graphics.DrawString(okulAdim, new System.Drawing.Font(new FontFamily("Segoe UI"), 12, FontStyle.Bold), Brushes.Black, new RectangleF(200, 120, 400, 50), format); //***METİN ORTALAMA

            string ogrenciBilgi = "Öğrencinin Numarası :   " + txtNo.Text + "                                                 " + "Görüşme Tarihi: " + dateTimePicker1.Text + "\n" + "\n" + "Öğrencinin Adı Soyadı :   " + txtAd.Text + "        " + "Öğrencinin Sınıfı :  " + txtSinif.Text + "\n" + "\n" + "Görüşme Türü :  " + cmbGorusmeTuru.Text;
            e.Graphics.DrawString(ogrenciBilgi, new System.Drawing.Font(new FontFamily("Segoe UI"), 9, FontStyle.Bold), Brushes.Black, new RectangleF(100, 180, 620, 365));

            StringFormat format2 = new StringFormat();
            format2.LineAlignment = StringAlignment.Center;

            string sorunAlaniCumlesi = "";
            for (int i = 0; i < (clistSorunAlanlari.CheckedItems.Count); i++)
            {
                sorunAlaniCumlesi += "\t\u2022 " + ((kaynakSorunAlanlari)clistSorunAlanlari.CheckedItems[i]).sorunAlani + "\n";
            }
            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 310, 650, 100));  //Sorun Alanları  
            e.Graphics.DrawString("Sorun Alanları", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 290, 650, 100));
            e.Graphics.DrawString(sorunAlaniCumlesi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 320, 650, 75), format2);


            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 440, 650, 180));  //Görüşme Konuları
            e.Graphics.DrawString("Görüşme Konusu", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 420, 650, 180));
            e.Graphics.DrawString(richTextBox1.Text, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 450, 650, 165), format2);

            string yapilacakCalismaCumlesi = "";
            for (int i = 0; i < (clistYapilacakCalismalar.CheckedItems.Count); i++)
            {
                yapilacakCalismaCumlesi += "\t\u2022 " + ((kaynakYapilacakCalismalar)clistYapilacakCalismalar.CheckedItems[i]).yapilacakCalisma + "\n";
            }

            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 650, 650, 150));  //Yapılacak Çalışmalar
            e.Graphics.DrawString("Yapılacak Çalışmalar", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 630, 650, 150));
            e.Graphics.DrawString(yapilacakCalismaCumlesi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 660, 650, 130), format2);

            string sonucCumlesi = "";
            for (int i = 0; i < (clistGorusmeSonuc.CheckedItems.Count); i++)
            {
                sonucCumlesi += "\t\u2022 " + ((kaynakSonucCumleleri)clistGorusmeSonuc.CheckedItems[i]).sonucCumlesi + "\n";
            }
            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 830, 650, 210));  //Sonuçlar  
            e.Graphics.DrawString("Sonuç, Görüş ve Öneriler", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 810, 650, 210));
            e.Graphics.DrawString(sonucCumlesi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 840, 620, 190), format2);


            string gorusmeYapanKisi = "Görüşmeyi Yapan" + "\n" + cmbGorusmeyiYapan.Text;
            e.Graphics.DrawString(gorusmeYapanKisi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(635, 1060, 130, 130));

        }

        private void UCuyeGorusme2_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DBkaynakVerileriGetir();
        }

        private void txtNo_TextChanged(object sender, EventArgs e)
        {
            btnGorusmeKaydet.Enabled = true;
            btnOgrenciGorusmeYazdir.Enabled = true;            
        }

        private void KontrolTemizleme()
        {
            dateTimePicker1.ResetText();
            cmbGorusmeTuru.SelectedIndex = 0;
            cmbGorusmeyiYapan.SelectedIndex = 0;
            cmbPdrAlanlari.SelectedIndex = 0;
            richTextBox1.ResetText();

            CheckedTemizle(clistGorusmeSonuc);
            CheckedTemizle(clistSorunAlanlari);
            CheckedTemizle(clistYapilacakCalismalar);            
        }
        
        private void CheckedTemizle(CheckedListBox kontrol)
        {
            kontrol.ClearSelected();
            for (int i = 0; i < kontrol.Items.Count; i++)
            {
                kontrol.SetItemChecked(i, false);
            }
        }
        



    }
}
