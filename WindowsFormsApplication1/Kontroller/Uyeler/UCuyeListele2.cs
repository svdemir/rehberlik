﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace rbsProje.Kontroller.Uyeler
{
    public partial class UCuyeListele2 : UserControl
    {
        public UCuyeListele2()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        public long ogrNo;

        private void UCuyeListele2_Load(object sender, EventArgs e)
        {
            textBox1.Focus();
            TemelOgreciBilgileriRenkDegistir();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var secilen = treeView1.SelectedNode;
            string secilenIsim = secilen.Text;
            int boslukIndex = secilenIsim.IndexOf(' ');
            if (boslukIndex > 0)
            {
                string ogrNoMetin = secilenIsim.Substring(0, secilenIsim.IndexOf(' '));
                ogrNo = Convert.ToInt64(ogrNoMetin);
                TemelOgreciBilgileriniTemizle();
                TemelOgrenciBilgileriniDoldur(ogrNo);
                DetayBilgileriTemizle();
                DetayliOgrenciBilgileriniDoldur(ogrNo);
                btnDetayGetir.Enabled = true;
            }
        }

        private void OgrencileriGetir()
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var liste = veritabanim.UYELER.GroupBy(p => p.SINIF).ToList();
                TreeviewDoldur(liste);
            }
        }

        private void TreeviewDoldur(List<IGrouping<string, UYELER>> liste)
        {
            treeView1.Nodes.Clear();
            if (liste.Count > 0)
            {
                foreach (var item in liste)
                {
                    treeView1.Nodes.Add(item.Key);
                    foreach (var subItem in item)
                    {
                        treeView1.Nodes[liste.IndexOf(item)].Nodes.Add(subItem.NO.ToString() + " - " + subItem.AD + " " + subItem.SOYAD);
                    }
                }
            }
            else
            {
                treeView1.Nodes.Add("Yok.");
                btnDetayGetir.Enabled = false;
            }
        }

        private void btnTumunuGetir_Click(object sender, EventArgs e)
        {
            TemelOgreciBilgileriniTemizle();
            OgrencileriGetir();
        }

        private void btnAra_Click(object sender, EventArgs e)
        {
            AramaIslemi();
        }

        private void AramaIslemi()
        {
            TemelOgreciBilgileriniTemizle();
            using (dbEntities veritabanim = new dbEntities())
            {
                string ogrAdi = textBox1.Text.ToUpper();
                var liste = veritabanim.UYELER.Where(u => u.AD.Contains(ogrAdi)).GroupBy(p => p.SINIF).ToList();
                TreeviewDoldur(liste);
            }
        }

        private void DbResimGüncelleme()
        {
            try
            {
                openFileDialog1.Title = "Resim Aç";
                openFileDialog1.Filter = "Jpeg Dosyası (*.jpg)|*.jpg|Gif Dosyası (*.gif)|*.gif|Png Dosyası (*.png)|*.png|Tif Dosyası (*.tif)|*.tif";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);

                    FileStream fs = new FileStream(openFileDialog1.FileName.ToString(), FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    byte[] resim = br.ReadBytes((int)fs.Length);
                    br.Close();
                    fs.Close();
                    br.Dispose();
                    fs.Dispose();

                    dbEntities veritabanim = new dbEntities();
                    var guncellenecekKayit = veritabanim.UYELER.Where(u => u.NO == ogrNo).SingleOrDefault();
                    guncellenecekKayit.resim = resim;
                    veritabanim.SaveChanges();
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Kayıt işlemi hatalı.", "Resim Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PictureBoxResimYerlestirme(byte[] resimDizisi)
        {
            if (resimDizisi != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(resimDizisi, 0, resimDizisi.Length);
                    Image img = Image.FromStream(ms);
                    pictureBox1.Image = img;
                }
            }
            else
            {
                pictureBox1.Image = null;
            }
        }

        private void btnDetayGetir_Click(object sender, EventArgs e)
        {
            tabControl1.Visible = true;
        }

        public void TemelOgrenciBilgileriniDoldur(long ogrNo)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var secilenKayit = veritabanim.UYELER.Where(u => u.NO == ogrNo).SingleOrDefault();
                txtAd.Text = secilenKayit.AD + " " + secilenKayit.SOYAD;
                txtCinsiyet.Text = secilenKayit.cinsiyet;
                txtDogumIL.Text = secilenKayit.dogumYeriIL;
                txtDogumIlce.Text = secilenKayit.dogumYeriIlce;
                txtDogumYil.Text = secilenKayit.dogumYili.ToString();
                txtKanGrubu.Text = secilenKayit.kanGrubu;
                txtNo.Text = secilenKayit.NO.ToString();
                txtRehberOgr.Text = secilenKayit.sinifRehberOgrt;
                txtSinif.Text = secilenKayit.SINIF;
                PictureBoxResimYerlestirme(secilenKayit.resim);
            }
        }

        public void DetayliOgrenciBilgileriniDoldur(long ogrNo)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var sorguVeli = veritabanim.tanitimFisiVeliBilgisi.Where(x => x.ogrNo == ogrNo).SingleOrDefault();
                if (sorguVeli != null)
                {
                    txtVeliAdSoyad.Text = sorguVeli.adSoyad;
                    txtVeliMeslek.Text = sorguVeli.meslek;
                    txtVeliTel.Text = sorguVeli.telefon;
                    txtVeliYakinligi.Text = sorguVeli.yakinligi;
                    richTextAdres.Text = sorguVeli.evAdresi;
                }


                var sorguBaba = veritabanim.tanitimFisiBabaBilgisi.Where(x => x.ogrNo == ogrNo).SingleOrDefault();
                if (sorguBaba != null)
                {
                    txtBabaAd.Text = sorguBaba.adSoyad;
                    txtBabaMeslek.Text = sorguBaba.meslek;
                    txtBabaTel.Text = sorguBaba.telefon;
                    cmbBirlik.Text = sorguBaba.birlikteAyri;
                    cmbOzUvey.Text = sorguBaba.ozUvey;
                    cmbSagOlu.Text = sorguBaba.sagOlu;
                    cmbBabaEngel.Text = sorguBaba.engelDurumu;
                }


                var sorguAnne = veritabanim.tanitimFisiAnneBilgisi.Where(x => x.ogrNo == ogrNo).SingleOrDefault();
                if (sorguAnne != null)
                {
                    txtAnneAd.Text = sorguAnne.adSoyad;
                    txtAnneMeslek.Text = sorguAnne.meslek;
                    txtAnneTel.Text = sorguAnne.telefon;
                    cmbAnneOz.Text = sorguAnne.ozUvey;
                    cmbAnneSag.Text = sorguAnne.sagOlu;
                    cmAnneBirlik.Text = sorguAnne.birlikteAyri;
                    cmbAnneEngel.Text = sorguAnne.engel;
                }

                var sorguGenelDurum = veritabanim.tanitimFisiGenelDurum.Where(x => x.ogrNo == ogrNo).SingleOrDefault();
                if (sorguGenelDurum != null)
                {
                    cmbAileCiddi.Text = sorguGenelDurum.aileCiddiRahatsizlik;
                    cmbAileProblem.Text = sorguGenelDurum.aileProblemlemi;
                    cmbOda.Text = sorguGenelDurum.calismaOdasi;
                    cmbMisafir.Text = sorguGenelDurum.evdeAileDisiYasayan;
                    cmbEvIsinma.Text = sorguGenelDurum.isinmaDurumu;
                    cmbOgrenciCiddi.Text = sorguGenelDurum.ogrenciCiddiRahatsizlik;

                    richOkulRahatsiz.Text = sorguGenelDurum.okuldaRahatsizlikDurumu;
                    cmbDestek.Text = sorguGenelDurum.okulDisiDestek;
                    txtOncekiOkul.Text = sorguGenelDurum.oncekiOkul;
                    cmbSinifTekrari.Text = sorguGenelDurum.sinifTekrari;
                    cmbEv.Text = sorguGenelDurum.yasadigiEv;
                    cmbYetenek.Text = sorguGenelDurum.yetenek;
                    txtMeslekAlan.Text = sorguGenelDurum.yonelecegiAlan;
                    cmbZararli.Text = sorguGenelDurum.zararliAliskanliklar;
                }

                var sorguSaglik = veritabanim.tanitimFisiSaglikDurumu.Where(x => x.ogrNo == ogrNo).SingleOrDefault();
                if (sorguSaglik != null)
                {
                    if (!string.IsNullOrEmpty(sorguSaglik.boy))
                    {
                        numericUpDown1.Value = Convert.ToInt32(sorguSaglik.boy);
                    }
                    cmbProtez.Text = sorguSaglik.cihazProtez;
                    cmbEngel.Text = sorguSaglik.engelDurumu;
                    cmbKaza.Text = sorguSaglik.kazaAmeliyat;
                    if (!string.IsNullOrEmpty(sorguSaglik.kilo))
                    {
                        numericUpDown2.Value = Convert.ToInt32(sorguSaglik.kilo);
                        numericUpDown2.Refresh();
                    }
                }

                var sorguOgrenme = veritabanim.tanitimFisiOgrenimDurumu.Where(x => x.ogrNo == ogrNo).SingleOrDefault();
                if (sorguOgrenme != null)
                {
                    txtBasariliDers.Text = sorguOgrenme.basariliDersler;
                    txtBasarisizDers.Text = sorguOgrenme.basarisizDersler;
                    richBosZaman.Text = sorguOgrenme.bosZamanEtkinlikleri;
                    richKulup.Text = sorguOgrenme.katilmakIstedigiKulup;
                    txtSinifRehber.Text = sorguOgrenme.sinifRehberOgretmeni;
                }



                var sorguKardes = veritabanim.tanitimFisiKardesBilgisi.Where(x => x.ogrNo == ogrNo).ToList();
                if (sorguKardes != null)
                {
                    GridTemizle();
                    for (int i = 0; i < sorguKardes.Count; i++)
                    {
                        dataGridView1.Rows.Add(sorguKardes[i].adSoyad, sorguKardes[i].dogumYili, sorguKardes[i].ozUvey, sorguKardes[i].sagOlu, sorguKardes[i].okul, sorguKardes[i].sinif);
                    }
                }



                var sorguEk = veritabanim.tanitimFisiEkAciklamalar.Where(x => x.ogrNo == ogrNo).SingleOrDefault();
                if (sorguEk != null)
                {
                    cArgo.Checked = Convert.ToBoolean(sorguEk.argoKullaniyor);
                    cArkadasSaygisiz.Checked = Convert.ToBoolean(sorguEk.arkadaslarinaSaygisiz);
                    cDaginik.Checked = Convert.ToBoolean(sorguEk.daginik);
                    cDersGec.Checked = Convert.ToBoolean(sorguEk.derseGecKaliyor);
                    cDersKonusuyor.Checked = Convert.ToBoolean(sorguEk.dersteKonusuyor);
                    cDikkat.Checked = Convert.ToBoolean(sorguEk.dikkatToplayamiyor);
                    cHaylaz.Checked = Convert.ToBoolean(sorguEk.haylaz);
                    cHircin.Checked = Convert.ToBoolean(sorguEk.hircin);
                    cKapanik.Checked = Convert.ToBoolean(sorguEk.iceKapanik);
                    cOgrtSaygisiz.Checked = Convert.ToBoolean(sorguEk.ogrtKarsiSaygisiz);
                    cOzBakim.Checked = Convert.ToBoolean(sorguEk.ozbakimGelismemis);
                    cSakin.Checked = Convert.ToBoolean(sorguEk.sakin);
                    cSaldırgan.Checked = Convert.ToBoolean(sorguEk.saldırgan);
                    cSinifKurallari.Checked = Convert.ToBoolean(sorguEk.sinifKuralUymuyor);
                    cOkulKurallari.Checked = Convert.ToBoolean(sorguEk.okulKurallarinaUymuyor);

                }

            }
        }

        private void GridTemizle()
        {
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
        }

        private void DetayBilgileriTemizle()
        {
            for (int i = 0; i < tabControl1.Controls.Count; i++)
            {
                foreach (var item in tabControl1.Controls[i].Controls)
                {
                    if (item is TextBox)
                    {
                        ((TextBox)item).Text = null;
                    }
                    if (item is CheckBox)
                    {
                        ((CheckBox)item).Checked = false;
                    }
                    if (item is ComboBox)
                    {
                        ((ComboBox)item).Text = null;
                    }
                    if (item is DataGridView)
                    {
                        ((DataGridView)item).DataSource = null;
                    }
                    if (item is DataGridView)
                    {
                        ((DataGridView)item).DataSource = null;
                    }
                    if (item is RichTextBox)
                    {
                        ((RichTextBox)item).Text = null;
                    }
                    if (item is NumericUpDown)
                    {
                        ((NumericUpDown)item).Value = 0;
                    }


                    foreach (var altKontrol in ((Control)item).Controls)            //groupbox içindeki kontroller için ayrıca döngü
                    {
                        if (altKontrol is TextBox)
                        {
                            ((TextBox)altKontrol).Text = null;
                        }
                        if (altKontrol is CheckBox)
                        {
                            ((CheckBox)altKontrol).Checked = false;
                        }
                        if (altKontrol is ComboBox)
                        {
                            ((ComboBox)altKontrol).Text = null;
                        }
                        if (altKontrol is DataGridView)
                        {
                            ((DataGridView)altKontrol).DataSource = null;
                        }
                        if (altKontrol is DataGridView)
                        {
                            ((DataGridView)altKontrol).DataSource = null;
                        }
                        if (altKontrol is RichTextBox)
                        {
                            ((RichTextBox)altKontrol).Text = null;
                        }
                        if (altKontrol is NumericUpDown)
                        {
                            ((NumericUpDown)altKontrol).Value = 0;
                        }

                    }
                }
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAra.PerformClick();
            }
        }

        private void TemelOgreciBilgileriniTemizle()
        {
            foreach (var item in groupBox2.Controls)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).Text = null;
                }
            }
            pictureBox1.Image = null;
        }

        private void TemelOgreciBilgileriRenkDegistir()
        {
            foreach (var item in groupBox2.Controls)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).BackColor = Color.White;
                }
            }
        }

        private void btnResimDuzenle_Click(object sender, EventArgs e)
        {
            DbResimGüncelleme();
        }

        private void btnAileKaydet_Click(object sender, EventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                tanitimFisiVeliBilgisi velim;
                var sorguVeli = veritabanim.tanitimFisiVeliBilgisi.Where(v => v.ogrNo == ogrNo).SingleOrDefault();
                if (sorguVeli != null)
                {
                    velim = sorguVeli;
                }
                else
                {
                    velim = new tanitimFisiVeliBilgisi();
                    veritabanim.tanitimFisiVeliBilgisi.Add(velim);
                }
                velim.ogrNo = ogrNo;
                velim.adSoyad = txtVeliAdSoyad.Text;
                velim.yakinligi = txtVeliYakinligi.Text.ToUpper();
                velim.evAdresi = richTextAdres.Text;
                velim.meslek = txtVeliMeslek.Text;
                velim.telefon = txtVeliTel.Text;



                tanitimFisiBabaBilgisi baba;
                var sorguBaba = veritabanim.tanitimFisiBabaBilgisi.Where(v => v.ogrNo == ogrNo).SingleOrDefault();
                if (sorguBaba != null)
                {
                    baba = sorguBaba;
                }
                else
                {
                    baba = new tanitimFisiBabaBilgisi();
                    veritabanim.tanitimFisiBabaBilgisi.Add(baba);
                }

                baba.ogrNo = ogrNo;
                baba.adSoyad = txtBabaAd.Text;
                baba.birlikteAyri = cmbBirlik.Text;
                baba.meslek = txtBabaMeslek.Text;
                baba.ozUvey = cmbOzUvey.Text;
                baba.sagOlu = cmbSagOlu.Text;
                baba.telefon = txtBabaTel.Text;
                baba.engelDurumu = cmbBabaEngel.Text;

                tanitimFisiAnneBilgisi anne;
                var sorguAnne = veritabanim.tanitimFisiAnneBilgisi.Where(v => v.ogrNo == ogrNo).SingleOrDefault();
                if (sorguAnne != null)
                {
                    anne = sorguAnne;
                }
                else
                {
                    anne = new tanitimFisiAnneBilgisi();
                    veritabanim.tanitimFisiAnneBilgisi.Add(anne);
                }

                anne.ogrNo = ogrNo;
                anne.adSoyad = txtAnneAd.Text;
                anne.birlikteAyri = cmbBirlik.Text;
                anne.meslek = txtAnneMeslek.Text;
                anne.ozUvey = cmbAnneOz.Text;
                anne.sagOlu = cmbAnneSag.Text;
                anne.telefon = txtAnneTel.Text;
                anne.engel = cmbAnneEngel.Text;

                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Kayıt işlemi başarılı.", "Aile Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception)
                {
                    MessageBox.Show("Kayıt işlemi hatalı.", "Aile Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnGenelKaydet_Click(object sender, EventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                tanitimFisiGenelDurum genelDurum;
                var sorguGenelDurum = veritabanim.tanitimFisiGenelDurum.Where(v => v.ogrNo == ogrNo).SingleOrDefault();
                if (sorguGenelDurum != null)
                {
                    genelDurum = sorguGenelDurum;
                }
                else
                {
                    genelDurum = new tanitimFisiGenelDurum();
                    veritabanim.tanitimFisiGenelDurum.Add(genelDurum);
                }

                genelDurum.aileCiddiRahatsizlik = lblAileCiddi.Text;
                genelDurum.aileProblemlemi = cmbAileProblem.Text;
                genelDurum.calismaOdasi = cmbOda.Text;
                genelDurum.evdeAileDisiYasayan = cmbMisafir.Text;
                genelDurum.isinmaDurumu = cmbEvIsinma.Text;
                genelDurum.ogrenciCiddiRahatsizlik = cmbOgrenciCiddi.Text;
                genelDurum.ogrNo = ogrNo;
                genelDurum.okuldaRahatsizlikDurumu = richOkulRahatsiz.Text;
                genelDurum.okulDisiDestek = cmbDestek.Text;
                genelDurum.oncekiOkul = txtOncekiOkul.Text;
                genelDurum.sinifTekrari = cmbSinifTekrari.Text;
                genelDurum.yasadigiEv = cmbEv.Text;
                genelDurum.yetenek = cmbYetenek.Text;
                genelDurum.yonelecegiAlan = txtMeslekAlan.Text;
                genelDurum.zararliAliskanliklar = cmbZararli.Text;

                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Kayıt işlemi başarılı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception)
                {
                    MessageBox.Show("Kayıt işlemi hatalı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnSaglikKaydet_Click(object sender, EventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                tanitimFisiSaglikDurumu saglikDurum;
                var sorgusaglikDurum = veritabanim.tanitimFisiSaglikDurumu.Where(v => v.ogrNo == ogrNo).SingleOrDefault();
                if (sorgusaglikDurum != null)
                {
                    saglikDurum = sorgusaglikDurum;
                }
                else
                {
                    saglikDurum = new tanitimFisiSaglikDurumu();
                    veritabanim.tanitimFisiSaglikDurumu.Add(saglikDurum);
                }

                saglikDurum.boy = numericUpDown1.Value.ToString();
                saglikDurum.cihazProtez = cmbProtez.Text;
                saglikDurum.engelDurumu = cmbEngel.Text;
                saglikDurum.kazaAmeliyat = cmbKaza.Text;
                saglikDurum.kilo = numericUpDown2.Value.ToString();
                saglikDurum.ogrNo = ogrNo;

                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Kayıt işlemi başarılı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("Kayıt işlemi hatalı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }


        }

        private void btnOgrDurumuKaydet_Click(object sender, EventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                tanitimFisiOgrenimDurumu ogrenmeDurum;
                var sorguOgrenmeDurum = veritabanim.tanitimFisiOgrenimDurumu.Where(v => v.ogrNo == ogrNo).SingleOrDefault();
                if (sorguOgrenmeDurum != null)
                {
                    ogrenmeDurum = sorguOgrenmeDurum;
                }
                else
                {
                    ogrenmeDurum = new tanitimFisiOgrenimDurumu();
                    veritabanim.tanitimFisiOgrenimDurumu.Add(ogrenmeDurum);
                }

                ogrenmeDurum.basariliDersler = txtBasariliDers.Text;
                ogrenmeDurum.basarisizDersler = txtBasarisizDers.Text;
                ogrenmeDurum.bosZamanEtkinlikleri = richBosZaman.Text;
                ogrenmeDurum.katilmakIstedigiKulup = richKulup.Text;
                ogrenmeDurum.ogrNo = ogrNo;
                ogrenmeDurum.sinifRehberOgretmeni = txtSinifRehber.Text;

                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Kayıt işlemi başarılı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("Kayıt işlemi hatalı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }

        }

        private void btnKardesKaydet_Click(object sender, EventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                tanitimFisiKardesBilgisi kardesDurum;
                var sorguKardesDurum = veritabanim.tanitimFisiKardesBilgisi.Where(v => v.ogrNo == ogrNo).ToList();
                if (sorguKardesDurum.Count > 0)
                {
                    veritabanim.tanitimFisiKardesBilgisi.RemoveRange(sorguKardesDurum);
                }
                dataGridView1.AllowUserToAddRows = false;
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    kardesDurum = new tanitimFisiKardesBilgisi();
                    if (dataGridView1.Rows[i].Cells[0].Value != null)
                    {
                        kardesDurum.adSoyad = dataGridView1.Rows[i].Cells[0].Value.ToString();
                    }
                    if (dataGridView1.Rows[i].Cells[1].Value != null)
                    {
                        kardesDurum.dogumYili = Convert.ToInt64(dataGridView1.Rows[i].Cells[1].Value);
                    }
                    if (dataGridView1.Rows[i].Cells[2].Value != null)
                    {
                        kardesDurum.ozUvey = dataGridView1.Rows[i].Cells[2].Value.ToString();
                    }
                    if (dataGridView1.Rows[i].Cells[3].Value != null)
                    {
                        kardesDurum.sagOlu = dataGridView1.Rows[i].Cells[3].Value.ToString();
                    }
                    if (dataGridView1.Rows[i].Cells[4].Value != null)
                    {
                        kardesDurum.okul = dataGridView1.Rows[i].Cells[4].Value.ToString();
                    }
                    if (dataGridView1.Rows[i].Cells[5].Value != null)
                    {
                        kardesDurum.sinif = dataGridView1.Rows[i].Cells[5].Value.ToString();
                    }
                    kardesDurum.ogrNo = ogrNo;
                    veritabanim.tanitimFisiKardesBilgisi.Add(kardesDurum);
                }
                dataGridView1.AllowUserToAddRows = true;

                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Kayıt işlemi başarılı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception)
                {
                    MessageBox.Show("Kayıt işlemi hatalı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }



        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                tanitimFisiEkAciklamalar ekAciklama;
                var sorguDurum = veritabanim.tanitimFisiEkAciklamalar.Where(v => v.ogrNo == ogrNo).SingleOrDefault();
                if (sorguDurum != null)
                {
                    ekAciklama = sorguDurum;
                }
                else
                {
                    ekAciklama = new tanitimFisiEkAciklamalar();
                    veritabanim.tanitimFisiEkAciklamalar.Add(ekAciklama);
                }

                ekAciklama.argoKullaniyor = cArgo.Checked.ToString();
                ekAciklama.arkadaslarinaSaygisiz = cArkadasSaygisiz.Checked.ToString();
                ekAciklama.daginik = cDaginik.Checked.ToString();
                ekAciklama.derseGecKaliyor = cDersGec.Checked.ToString();
                ekAciklama.dersteKonusuyor = cDersKonusuyor.Checked.ToString();
                ekAciklama.dikkatToplayamiyor = cDikkat.Checked.ToString();
                ekAciklama.haylaz = cHaylaz.Checked.ToString();
                ekAciklama.hircin = cHircin.Checked.ToString();
                ekAciklama.iceKapanik = cKapanik.Checked.ToString();
                ekAciklama.ogrNo = ogrNo;
                ekAciklama.ogrtKarsiSaygisiz = cOgrtSaygisiz.Checked.ToString();
                ekAciklama.ozbakimGelismemis = cOzBakim.Checked.ToString();
                ekAciklama.sakin = cSakin.Checked.ToString();
                ekAciklama.saldırgan = cSaldırgan.Checked.ToString();
                ekAciklama.sinifKuralUymuyor = cSinifKurallari.Checked.ToString();
                ekAciklama.okulKurallarinaUymuyor = cOkulKurallari.Checked.ToString();

                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Kayıt işlemi başarılı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("Kayıt işlemi hatalı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }



        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                try
                {
                    dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
                }
                catch (Exception)
                {
                    MessageBox.Show("Silme işlemi hatalı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
