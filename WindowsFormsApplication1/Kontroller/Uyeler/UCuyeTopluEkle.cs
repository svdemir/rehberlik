﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SQLite;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace rbsProje.Kontroller.Uyeler
{
    public partial class UCuyeTopluEkle : UserControl
    {
        public UCuyeTopluEkle()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        string DosyaYolu = "";

        DataSet ds = new DataSet();
        DataTable dt; //hatalı kayıtlar için oluşturulan tablo.


        #region Excel Okuma İşlemleri

        private void btnKitapListesiGetir_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.RestoreDirectory = true;
            file.CheckFileExists = false;
            file.Title = "Dosyası Seçiniz..";
            if (file.ShowDialog() == DialogResult.OK)
            {
                DosyaYolu = file.FileName;
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void VerileriGetir()
        {
            string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + DosyaYolu + "; Extended Properties=Excel 12.0 Xml";
            //string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + DosyaYolu + "; Extended Properties=Excel 12.0";

            OleDbConnection baglanti = new OleDbConnection(connectionString);

            try
            {
                baglanti.Open();
                using (var adapter = new OleDbDataAdapter("SELECT * FROM [Sayfa1$]", connectionString))
                {
                    adapter.Fill(ds);
                    btnKitapListesiGetir.Enabled = false;
                    btnKaydet.Enabled = true;
                    baglanti.Close();
                    baglanti.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Excel dosyası hatalı." + ex.Message, "Üye Listesi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            VerileriGetir();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (ds.Tables.Count != 0)
            {
                dataGridView1.DataSource = ds.Tables[0];
                dataGridView1.AllowUserToAddRows = false;
                lblKayit.Text = dataGridView1.RowCount.ToString();
                lblKayit.Refresh();
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        #endregion


        #region Database Kayıt İşlemleri

        private void VerileriYaz2()
        {
            dt = ds.Tables[0].Clone();
            int hataSayisi = 0;

            var dbcon = new SQLiteConnection("Data Source=db.sqlite;Version=3;");
            dbcon.Open();
            SQLiteCommand sqlComm;
            sqlComm = new SQLiteCommand("begin", dbcon);
            sqlComm.ExecuteNonQuery();
            //---INSIDE LOOP
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                DataGridViewRow Row = dataGridView1.Rows[i];
                string komut = @"INSERT INTO UYELER ([NO], [AD], [SOYAD], [SINIF]) VALUES ('" + Row.Cells[0].Value.ToString() + "','" + Row.Cells[1].Value.ToString() + "','" + Row.Cells[2].Value.ToString() + "','" + Row.Cells[3].Value.ToString() + "')";
                sqlComm = new SQLiteCommand(komut, dbcon);


                try
                {
                    sqlComm.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    DataRow rw = dt.NewRow();
                    rw.ItemArray = new object[] { Row.Cells[0].Value, Row.Cells[1].Value, Row.Cells[2].Value, Row.Cells[3].Value };
                    dt.Rows.Add(rw);
                    hataSayisi++;
                    label2.Text = hataSayisi.ToString();
                    label2.Refresh();
                }
            }
            //---END LOOP
            sqlComm = new SQLiteCommand("end", dbcon);
            sqlComm.ExecuteNonQuery();
            dbcon.Close();
            dbcon.Dispose();
            sqlComm.Dispose();
        }

        private void DbResimGüncelleme()
        {
            try
            {              
                    

                                   

            }
            catch (Exception)
            {
                MessageBox.Show("Kayıt işlemi hatalı.", "Resim Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void VerileriYaz()
        {
            dt = ds.Tables[0].Clone();
            int hataSayisi = 0;

            dbEntities veritabanim = new dbEntities();


            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                DataGridViewRow Row = dataGridView1.Rows[i];

                UYELER yeniOgrenci = new UYELER();
                yeniOgrenci.NO = Convert.ToInt64(Row.Cells[1].Value);
                yeniOgrenci.AD = Row.Cells[2].Value.ToString();
                yeniOgrenci.SOYAD = Row.Cells[3].Value.ToString();
                yeniOgrenci.SINIF = Row.Cells[0].Value.ToString();
                yeniOgrenci.cinsiyet = Row.Cells[4].Value.ToString();
                yeniOgrenci.dogumYeriIL = Row.Cells[5].Value.ToString();
                yeniOgrenci.dogumYeriIlce = Row.Cells[6].Value.ToString();

                //var yil = Row.Cells[7].Value.ToString().Substring(0,Row.Cells[7].Value.ToString().Length - 9).Substring(Row.Cells[7].Value.ToString().Length - 4,4);         // 4 çıkar 4 al
                //string yil = "2009"         //düzenlecek  boş satırlar sorunlu;
                //yeniOgrenci.dogumYili = Int32.Parse(yil);

                yeniOgrenci.kanGrubu = Row.Cells[8].Value.ToString();

                //resim işlemi
                FileStream fs = new FileStream(Row.Cells[42].Value.ToString(), FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] resim = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                br.Dispose();
                fs.Dispose();

                yeniOgrenci.resim = resim;

                veritabanim.UYELER.Add(yeniOgrenci);

                tanitimFisiGenelDurum uyeGenelBilgi = new tanitimFisiGenelDurum();
                uyeGenelBilgi.ogrNo = Convert.ToInt64(Row.Cells[1].Value);
                uyeGenelBilgi.zararliAliskanliklar = Row.Cells[9].Value.ToString();
                uyeGenelBilgi.aileProblemlemi = Row.Cells[10].Value.ToString();
                uyeGenelBilgi.yasadigiEv = Row.Cells[11].Value.ToString();
                uyeGenelBilgi.isinmaDurumu = Row.Cells[12].Value.ToString();
                uyeGenelBilgi.sinifTekrari = Row.Cells[13].Value.ToString();
                uyeGenelBilgi.calismaOdasi = Row.Cells[14].Value.ToString();
                uyeGenelBilgi.ogrenciCiddiRahatsizlik = Row.Cells[15].Value.ToString();
                uyeGenelBilgi.aileCiddiRahatsizlik = Row.Cells[16].Value.ToString();
                uyeGenelBilgi.evdeAileDisiYasayan = Row.Cells[17].Value.ToString();
                veritabanim.tanitimFisiGenelDurum.Add(uyeGenelBilgi);

                tanitimFisiSaglikDurumu uyeSaglik = new tanitimFisiSaglikDurumu();
                uyeSaglik.ogrNo = Convert.ToInt64(Row.Cells[1].Value);
                uyeSaglik.boy = Row.Cells[18].Value.ToString();
                uyeSaglik.kilo = Row.Cells[19].Value.ToString();
                uyeSaglik.engelDurumu = Row.Cells[20].Value.ToString();
                uyeSaglik.cihazProtez = Row.Cells[21].Value.ToString();
                uyeSaglik.kazaAmeliyat = Row.Cells[22].Value.ToString();
                veritabanim.tanitimFisiSaglikDurumu.Add(uyeSaglik);

                string kardesGenelVeri = Row.Cells[23].Value.ToString();
                string[] kardesListe = kardesGenelVeri.Split('@');
                for (int k = 0; k < kardesListe.Length; k++)
                {
                    if (kardesListe[k] != null && kardesListe[k] != "")
                    {
                        tanitimFisiKardesBilgisi kardes = new tanitimFisiKardesBilgisi();
                        string[] kardesOzelBilgi = kardesListe[k].Split('-');
                        kardes.ogrNo = Convert.ToInt64(Row.Cells[1].Value);
                        kardes.adSoyad = kardesOzelBilgi[0].ToString();
                        var kardesYil = kardesOzelBilgi[1].ToString().Substring(kardesOzelBilgi[1].Length - 4, 4);
                        kardes.dogumYili = Convert.ToInt32(kardesYil);
                        kardes.dogumYeriIlce = kardesOzelBilgi[2].ToString();
                        veritabanim.tanitimFisiKardesBilgisi.Add(kardes);
                    }
                }

                tanitimFisiVeliBilgisi velisi = new tanitimFisiVeliBilgisi();
                velisi.ogrNo = Convert.ToInt64(Row.Cells[1].Value);
                velisi.adSoyad = Row.Cells[24].Value.ToString();
                velisi.yakinligi = Row.Cells[25].Value.ToString();
                velisi.meslek = Row.Cells[26].Value.ToString();
                velisi.telefon = Row.Cells[27].Value.ToString();
                veritabanim.tanitimFisiVeliBilgisi.Add(velisi);

                tanitimFisiBabaBilgisi babasi = new tanitimFisiBabaBilgisi();
                babasi.ogrNo = Convert.ToInt64(Row.Cells[1].Value);
                babasi.adSoyad = Row.Cells[28].Value.ToString();
                babasi.meslek = Row.Cells[29].Value.ToString();
                babasi.telefon = Row.Cells[30].Value.ToString();
                babasi.ozUvey = Row.Cells[31].Value.ToString();
                babasi.sagOlu = Row.Cells[32].Value.ToString();
                babasi.birlikteAyri = Row.Cells[33].Value.ToString();
                babasi.engelDurumu = Row.Cells[34].Value.ToString();
                veritabanim.tanitimFisiBabaBilgisi.Add(babasi);

                tanitimFisiAnneBilgisi annesi = new tanitimFisiAnneBilgisi();
                annesi.ogrNo = Convert.ToInt64(Row.Cells[1].Value);
                annesi.adSoyad = Row.Cells[35].Value.ToString();
                annesi.meslek = Row.Cells[36].Value.ToString();
                annesi.telefon = Row.Cells[37].Value.ToString();
                annesi.ozUvey = Row.Cells[38].Value.ToString();
                annesi.sagOlu = Row.Cells[39].Value.ToString();
                annesi.birlikteAyri = Row.Cells[40].Value.ToString();
                annesi.engel = Row.Cells[41].Value.ToString();
                veritabanim.tanitimFisiAnneBilgisi.Add(annesi);



                try
                {
                    veritabanim.SaveChanges();
                }
                catch (Exception ex)
                {
                    DataRow rw = dt.NewRow();
                    rw.ItemArray = new object[] { Row.Cells[0].Value, Row.Cells[1].Value, Row.Cells[2].Value, Row.Cells[3].Value, Row.Cells[4].Value, Row.Cells[5].Value, Row.Cells[6].Value, Row.Cells[7].Value, Row.Cells[8].Value, Row.Cells[9].Value, Row.Cells[10].Value, Row.Cells[11].Value, Row.Cells[12].Value, Row.Cells[13].Value };
                    dt.Rows.Add(rw);
                    hataSayisi++;
                    label2.Text = hataSayisi.ToString();
                    label2.Refresh();
                }
            }
            veritabanim.Dispose();
        }



        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            VerileriYaz();
        }

        private void backgroundWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }


        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            dataGridView1.DataSource = dt;
            dataGridView1.Refresh();
            lblBasarili.Text = (Convert.ToInt32(lblKayit.Text) - Convert.ToInt32(label2.Text)).ToString();
            lblBasarili.Refresh();
            MessageBox.Show("Aktarma işlemi tamamlandı.", "Üye Listesi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            ds.Dispose();
            dt.Dispose();
            this.ParentForm.Enabled = true;


        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            this.ParentForm.Enabled = false;
            backgroundWorker2.RunWorkerAsync();

        }


        #endregion

        private void btnSablon_Click(object sender, EventArgs e)
        {

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xlsx)|*.xlsx";
            sfd.FileName = "uyelerSablon.xlsx";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                var a = rbsProje.Properties.Resources.uyelerSablon;
                FileStream fileStream = new FileStream(sfd.FileName, FileMode.Create);
                fileStream.Write(a, 0, a.Length);
                fileStream.Close();
                fileStream.Dispose();
            }
            MessageBox.Show("Şablon indirme işlemi tamamlandı.", "Üye Listesi", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
    }
}
