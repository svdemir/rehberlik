﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;

namespace rbsProje.Kontroller.Uyeler
{
    public partial class UCuyeTopluDuzenle : UserControl
    {
        public UCuyeTopluDuzenle()
        {
            InitializeComponent();
        }

        SQLiteDataAdapter adapter;
        DataSet ds;

        private void btnKitapListesiGetir_Click(object sender, EventArgs e)
        {
            DBVerileriGetir();
            dataGridView1.Columns.Remove("sinif");
            dataGridView1.Columns.Remove("resim");
            GridDüzenle();
        }

        private void GridDüzenle()
        {
            if (ds.Tables.Count>0)
            {
                string komut2 = "SELECT * FROM SINIFLAR";
                DataSet ds2 = Temel_Metodlar.SqlKomutCalistirSorgu(komut2);
                DataGridViewComboBoxColumn columnSinif = new DataGridViewComboBoxColumn();
                columnSinif.DisplayIndex = 3;
                columnSinif.HeaderText = "Sınıfı";
                columnSinif.DataSource = ds2.Tables[0];
                columnSinif.DataPropertyName = "SINIF";
                columnSinif.DisplayMember = "SINIF";
                dataGridView1.Columns.Add(columnSinif);
                ds2.Dispose();


                dataGridView1.Columns[0].HeaderText = "No";
                dataGridView1.Columns[1].HeaderText = "Adı";
                dataGridView1.Columns[2].HeaderText = "Soyadı";
                dataGridView1.Columns[3].HeaderText = "İli";
                dataGridView1.Columns[4].HeaderText = "İlçesi";
                dataGridView1.Columns[5].HeaderText = "D. Yılı";
                dataGridView1.Columns[6].HeaderText = "Cinsiyet";
                dataGridView1.Columns[7].HeaderText = "Reh. Öğrt.";
                dataGridView1.Columns[8].HeaderText = "Kan Grubu";


                dataGridView1.Columns[0].Width = 70;
                dataGridView1.Columns[1].Width = 150;
                dataGridView1.Columns[2].Width = 140;
                dataGridView1.Columns[3].Width = 80;


                dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                btnKitapListesiGetir.Enabled = false;
                lblKitapSayisi.Text = (dataGridView1.Rows.Count - 1).ToString();
                groupBox1.Visible = true;
                btnTopluGuncelle.Enabled = true;
                btnSil.Enabled = true;
            }
        }

        private void btnTopluGuncelle_Click(object sender, EventArgs e)
        {
            try
            {
                adapter.Update(ds);
                MessageBox.Show("Üye listesi başarı ile güncellendi.", "Üye Toplu Düzenle", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DBVerileriGetir();
                dataGridView1.Columns.Remove("resim");
           
            }
            catch (Exception  )
            {
                MessageBox.Show("Üye listesi güncelleme hatası. Alanları kontrol edip, tekrar deneyiniz.", "Üye Toplu Düzenle", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Temel_Metodlar.Numaralandir(dataGridView1);
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Silme işlemini onaylıyor musunuz?", "Silme İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (cevap == DialogResult.Yes)
            {
                try
                {
                    string silinecekUyeNo = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    SQLiteCommand command = new SQLiteCommand("DELETE FROM UYELER WHERE NO=@1");
                    command.Parameters.AddWithValue("@1", silinecekUyeNo);
                    Temel_Metodlar.SqlKomutCalistir(command);
                    command.Dispose();
                    MessageBox.Show("Silme işlemi başarılı.", "Toplu Üye Düzenleme.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DBVerileriGetir();
                    dataGridView1.Columns.Remove("resim");

                }
                catch (Exception)
                {
                    MessageBox.Show("Silme işlemi başarısız.", "Toplu Üye Düzenleme.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool DBVerileriGetir()
        {
            string komut = "SELECT * FROM UYELER";

            var DBbaglanti = new SQLiteConnection("Data Source=db.sqlite;Version=3;");
            SQLiteCommand command = new SQLiteCommand(komut, DBbaglanti);
            DBbaglanti.Open();
            adapter = new SQLiteDataAdapter(command);
            ds = new DataSet();
            try
            {
                adapter.Fill(ds);
                SQLiteCommandBuilder cb = new SQLiteCommandBuilder(adapter);
                dataGridView1.DataSource = ds.Tables[0];
                DBbaglanti.Close();
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("Üye listesi veritabanından getirilemedi.", "Üye İşlemleri Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DBbaglanti.Close();
                return false;
            }            
        }
    }
}
