﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace rbsProje.Kontroller.Veli
{
    public partial class UCveliZiyaret : UserControl
    {
        public UCveliZiyaret()
        {
            InitializeComponent();
        }

        public void btnGetir_Click(object sender, EventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var sorgu = veritabanim.UYELER.Where(u => u.NO == numericUpDown1.Value).SingleOrDefault();
                if (sorgu != null)
                {
                    lblOgrenci.Text = sorgu.AD + " " + sorgu.SOYAD;
                }
                else
                {
                    lblOgrenci.Text = "";
                }
                var sorgu2 = veritabanim.tanitimFisiVeliBilgisi.Where(u => u.ogrNo == numericUpDown1.Value).SingleOrDefault();
                if (sorgu2 != null)
                {
                    lblVeli.Text = sorgu2.adSoyad;
                }
                else
                {
                    lblVeli.Text = "";
                }
            }
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            dataGridView1.AllowUserToAddRows = false;
            using (dbEntities veritabanim = new dbEntities())
            {
                gorusmeVeliZiyaret yeniZiyaret = new gorusmeVeliZiyaret();
                yeniZiyaret.mevcutDurum = richMevcutDurum.Text;
                yeniZiyaret.ogrNo = Convert.ToInt64(numericUpDown1.Value);
                yeniZiyaret.sonucDegerlendirme = richSonuc.Text;
                yeniZiyaret.tarih = dateTimePicker1.Value.ToString("yyyyMMdd");
                string ziyaretciler = "";
                for (int i = 0; i < dataGridView1.RowCount; i++)
                {
                    if (dataGridView1.Rows[i].Cells[0].Value == null)
                    {
                        dataGridView1.Rows[i].Cells[0].Value = "";
                    }
                    if (dataGridView1.Rows[i].Cells[1].Value == null)
                    {
                        dataGridView1.Rows[i].Cells[1].Value = "";
                    }
                    if (dataGridView1.Rows[i].Cells[0].Value.ToString() == "" && dataGridView1.Rows[i].Cells[1].Value.ToString() == "")
                    {

                    }
                    else
                    {
                        ziyaretciler += dataGridView1.Rows[i].Cells[0].Value.ToString() + "/ " + dataGridView1.Rows[i].Cells[1].Value.ToString() + ",";
                    }                    
                }
                yeniZiyaret.ziyaretEdenler = ziyaretciler;
                yeniZiyaret.ziyaretNedeni = richZiyaretNedeni.Text;
                veritabanim.gorusmeVeliZiyaret.Add(yeniZiyaret);
                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Ziyaret kayıt işlemi başarılı.", "Ziyaret Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView1.AllowUserToAddRows = true;
                    richMevcutDurum.Clear();
                    richSonuc.Clear();
                    richZiyaretNedeni.Clear();
                    dataGridView1.Rows.Clear();
                    numericUpDown1.Value = 0;
                    lblOgrenci.Text = "";
                    lblVeli.Text = "";
                }
                catch (Exception)
                {
                    MessageBox.Show("Ziyaret kayıt işlemi hatalı.", "Ziyaret Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }            
        }

        private void btnYazdir_Click(object sender, EventArgs e)
        {
            dataGridView1.AllowUserToAddRows = false;
            PrintDocument doc = new PrintDocument();
            doc.PrintPage += new PrintPageEventHandler(doc_PrintPage);
            PrintPreviewDialog pdg = new PrintPreviewDialog();
            pdg.Document = doc;
            pdg.Height = 700;
            pdg.Width = 900;
            pdg.Show();
        }

        private void doc_PrintPage(object sender, PrintPageEventArgs e)
        {

            //arkaplan çalışma
            //e.Graphics.DrawImage(Image.FromFile("back.jpg"), e.Graphics.VisibleClipBounds);



            e.Graphics.DrawRectangle(new Pen(Color.Black, 3), new Rectangle(60, 80, 700, 1040));  //Genel Çerçeve
            e.Graphics.DrawString("VELİ ZİYARETİ", new System.Drawing.Font(new FontFamily("Segoe UI"), 16, FontStyle.Bold), Brushes.Black, new RectangleF(320, 100, 620, 365));

            string okulAdim;
            using (dbEntities veritabanim = new dbEntities())
            {
                okulAdim = veritabanim.ayarlar.FirstOrDefault().OKULADI;
            }

            StringFormat format = new StringFormat();
            format.LineAlignment = StringAlignment.Center;
            format.Alignment = StringAlignment.Center;
            e.Graphics.DrawString(okulAdim, new System.Drawing.Font(new FontFamily("Segoe UI"), 12, FontStyle.Bold), Brushes.Black, new RectangleF(200, 120, 400, 50), format); //***METİN ORTALAMA

            string ogrenciBilgi = "Öğrencinin Numarası :   " + numericUpDown1.Value.ToString() + "                                         " + "Görüşme Tarihi: " + dateTimePicker1.Text + "\n" + "\n" + "Öğrencinin Adı Soyadı :   " + lblOgrenci.Text + "             " + "\n" + "\n" + "Velinin Adı Soyadı :  " + lblVeli.Text;
            e.Graphics.DrawString(ogrenciBilgi, new System.Drawing.Font(new FontFamily("Segoe UI"), 9, FontStyle.Bold), Brushes.Black, new RectangleF(100, 180, 620, 365));

            StringFormat format2 = new StringFormat();
            format2.LineAlignment = StringAlignment.Center;

            string ziyareteKatılanlar = "";
            for (int i = 0; i < (dataGridView1.RowCount); i++)
            {
                if (dataGridView1.Rows[i].Cells[0].Value == null)
                {
                    dataGridView1.Rows[i].Cells[0].Value = "";
                }
                if (dataGridView1.Rows[i].Cells[1].Value == null)
                {
                    dataGridView1.Rows[i].Cells[1].Value = "";
                }
                ziyareteKatılanlar += "\t\u2022 " + dataGridView1.Rows[i].Cells[0].Value.ToString() + " / " + dataGridView1.Rows[i].Cells[1].Value.ToString() + "\n";
            }
            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 330, 650, 140));  
            e.Graphics.DrawString("Ziyarete Katılanlar", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 310, 650, 140));
            e.Graphics.DrawString(ziyareteKatılanlar, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 340, 650, 130), format2);


            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 495, 650, 180));  
            e.Graphics.DrawString("Ziyaret Nedeni", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 475, 650, 180));
            e.Graphics.DrawString(richZiyaretNedeni.Text, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 505, 650, 170), format2);


            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 700, 650, 140)); 
            e.Graphics.DrawString("Mevcut Durum", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 680, 650, 140));
            e.Graphics.DrawString(richMevcutDurum.Text, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 710, 650, 130), format2);


            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 870, 650, 180));  //Sonuçlar  
            e.Graphics.DrawString("Sonuç, Görüş ve Değerlendirmeler", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 850, 650, 18));
            e.Graphics.DrawString(richSonuc.Text, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 880, 620, 150), format2);


            //string gorusmeYapanKisi = "Görüşmeyi Yapan" + "\n" + cmbGorusmeyiYapan.Text;
            //e.Graphics.DrawString(gorusmeYapanKisi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(635, 1070, 130, 130));

        }

    }
}
