﻿namespace rbsProje.Kontroller.Veli
{
    partial class UCveliZiyaret
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.lblOgrenci = new System.Windows.Forms.Label();
            this.lblVeli = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGetir = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.richZiyaretNedeni = new System.Windows.Forms.RichTextBox();
            this.richMevcutDurum = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.richSonuc = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnYazdir = new System.Windows.Forms.Button();
            this.btnVeliZiyaretKaydet = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.lblOgrenci);
            this.groupBox1.Controls.Add(this.lblVeli);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnGetir);
            this.groupBox1.Location = new System.Drawing.Point(37, 18);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(605, 126);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(170, 26);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(160, 25);
            this.numericUpDown1.TabIndex = 1;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblOgrenci
            // 
            this.lblOgrenci.AutoSize = true;
            this.lblOgrenci.Location = new System.Drawing.Point(200, 68);
            this.lblOgrenci.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOgrenci.Name = "lblOgrenci";
            this.lblOgrenci.Size = new System.Drawing.Size(0, 17);
            this.lblOgrenci.TabIndex = 1;
            // 
            // lblVeli
            // 
            this.lblVeli.AutoSize = true;
            this.lblVeli.Location = new System.Drawing.Point(200, 95);
            this.lblVeli.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblVeli.Name = "lblVeli";
            this.lblVeli.Size = new System.Drawing.Size(0, 17);
            this.lblVeli.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(69, 68);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "Öğrenci Adı Soyadı";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(69, 95);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Veli Adı Soyadı";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 29);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Öğrenci No";
            // 
            // btnGetir
            // 
            this.btnGetir.Image = global::rbsProje.Properties.Resources.uye1;
            this.btnGetir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGetir.Location = new System.Drawing.Point(379, 19);
            this.btnGetir.Margin = new System.Windows.Forms.Padding(4);
            this.btnGetir.Name = "btnGetir";
            this.btnGetir.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnGetir.Size = new System.Drawing.Size(100, 49);
            this.btnGetir.TabIndex = 0;
            this.btnGetir.Text = "     Getir";
            this.btnGetir.UseVisualStyleBackColor = true;
            this.btnGetir.Click += new System.EventHandler(this.btnGetir_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 281);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ziyaret Nedeni";
            // 
            // richZiyaretNedeni
            // 
            this.richZiyaretNedeni.Location = new System.Drawing.Point(14, 302);
            this.richZiyaretNedeni.Name = "richZiyaretNedeni";
            this.richZiyaretNedeni.Size = new System.Drawing.Size(685, 111);
            this.richZiyaretNedeni.TabIndex = 2;
            this.richZiyaretNedeni.Text = "";
            // 
            // richMevcutDurum
            // 
            this.richMevcutDurum.Location = new System.Drawing.Point(14, 443);
            this.richMevcutDurum.Name = "richMevcutDurum";
            this.richMevcutDurum.Size = new System.Drawing.Size(685, 112);
            this.richMevcutDurum.TabIndex = 3;
            this.richMevcutDurum.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 420);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Mevcut Durum";
            // 
            // richSonuc
            // 
            this.richSonuc.Location = new System.Drawing.Point(14, 590);
            this.richSonuc.Name = "richSonuc";
            this.richSonuc.Size = new System.Drawing.Size(685, 123);
            this.richSonuc.TabIndex = 4;
            this.richSonuc.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 567);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Sonuç Değerlendirme";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(271, -6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 30);
            this.label5.TabIndex = 5;
            this.label5.Text = "Veli Ziyareti";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "Ziyarete Katılanlar";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.dataGridView1.Location = new System.Drawing.Point(14, 188);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(363, 84);
            this.dataGridView1.TabIndex = 6;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Adı Soyadı";
            this.Column1.Name = "Column1";
            this.Column1.Width = 150;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Ünvanı";
            this.Column2.Name = "Column2";
            this.Column2.Width = 130;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dateTimePicker1);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.richSonuc);
            this.groupBox2.Controls.Add(this.btnYazdir);
            this.groupBox2.Controls.Add(this.btnVeliZiyaretKaydet);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.richZiyaretNedeni);
            this.groupBox2.Controls.Add(this.richMevcutDurum);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(10, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(718, 727);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(434, 185);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 25);
            this.dateTimePicker1.TabIndex = 7;
            // 
            // btnYazdir
            // 
            this.btnYazdir.Image = global::rbsProje.Properties.Resources.printer2;
            this.btnYazdir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnYazdir.Location = new System.Drawing.Point(557, 223);
            this.btnYazdir.Margin = new System.Windows.Forms.Padding(4);
            this.btnYazdir.Name = "btnYazdir";
            this.btnYazdir.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnYazdir.Size = new System.Drawing.Size(100, 49);
            this.btnYazdir.TabIndex = 0;
            this.btnYazdir.Text = "       Yazdır";
            this.btnYazdir.UseVisualStyleBackColor = true;
            this.btnYazdir.Click += new System.EventHandler(this.btnYazdir_Click);
            // 
            // btnVeliZiyaretKaydet
            // 
            this.btnVeliZiyaretKaydet.Image = global::rbsProje.Properties.Resources.kaydet3;
            this.btnVeliZiyaretKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVeliZiyaretKaydet.Location = new System.Drawing.Point(417, 223);
            this.btnVeliZiyaretKaydet.Margin = new System.Windows.Forms.Padding(4);
            this.btnVeliZiyaretKaydet.Name = "btnVeliZiyaretKaydet";
            this.btnVeliZiyaretKaydet.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnVeliZiyaretKaydet.Size = new System.Drawing.Size(100, 49);
            this.btnVeliZiyaretKaydet.TabIndex = 0;
            this.btnVeliZiyaretKaydet.Text = "        Kaydet";
            this.btnVeliZiyaretKaydet.UseVisualStyleBackColor = true;
            this.btnVeliZiyaretKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(431, 160);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 17);
            this.label9.TabIndex = 1;
            this.label9.Text = "Ziyaret Tarihi";
            // 
            // UCveliZiyaret
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UCveliZiyaret";
            this.Size = new System.Drawing.Size(741, 751);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Button btnVeliZiyaretKaydet;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.NumericUpDown numericUpDown1;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnGetir;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.RichTextBox richZiyaretNedeni;
        public System.Windows.Forms.RichTextBox richMevcutDurum;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.RichTextBox richSonuc;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label lblOgrenci;
        public System.Windows.Forms.Label lblVeli;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        public System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.DateTimePicker dateTimePicker1;
        public System.Windows.Forms.Button btnYazdir;
        public System.Windows.Forms.Label label9;
    }
}
