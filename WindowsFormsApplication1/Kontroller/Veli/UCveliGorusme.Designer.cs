﻿namespace rbsProje.Kontroller.Veli
{
    partial class UCveliGorusme
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnOgrenciGorusmeYazdir = new System.Windows.Forms.Button();
            this.btnGorusmeKaydet = new System.Windows.Forms.Button();
            this.clistGorusmeSonuc = new System.Windows.Forms.CheckedListBox();
            this.clistYapilacakCalismalar = new System.Windows.Forms.CheckedListBox();
            this.clistSorunAlanlari = new System.Windows.Forms.CheckedListBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cmbGorusmeyiYapan = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lblVeliGorusmeBaslik = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grbVeli.SuspendLayout();
            this.grbBaba.SuspendLayout();
            this.grbAnne.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnResimDuzenle
            // 
            this.btnResimDuzenle.Location = new System.Drawing.Point(636, 666);
            this.btnResimDuzenle.Visible = false;
            // 
            // btnDetayGetir
            // 
            this.btnDetayGetir.Location = new System.Drawing.Point(25, 188);
            // 
            // lblListeleBaslik
            // 
            this.lblListeleBaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblListeleBaslik.Location = new System.Drawing.Point(13, 23);
            this.lblListeleBaslik.Size = new System.Drawing.Size(127, 21);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblVeliGorusmeBaslik);
            this.groupBox1.Location = new System.Drawing.Point(3, 6);
            this.groupBox1.Size = new System.Drawing.Size(1009, 363);
            this.groupBox1.Controls.SetChildIndex(this.btnTumunuGetir, 0);
            this.groupBox1.Controls.SetChildIndex(this.btnAra, 0);
            this.groupBox1.Controls.SetChildIndex(this.pictureBox1, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl1, 0);
            this.groupBox1.Controls.SetChildIndex(this.treeView1, 0);
            this.groupBox1.Controls.SetChildIndex(this.textBox1, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.btnResimDuzenle, 0);
            this.groupBox1.Controls.SetChildIndex(this.groupBox2, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblVeliGorusmeBaslik, 0);
            // 
            // treeView1
            // 
            this.treeView1.LineColor = System.Drawing.Color.Black;
            this.treeView1.Location = new System.Drawing.Point(10, 128);
            this.treeView1.Size = new System.Drawing.Size(231, 219);
            // 
            // btnTumunuGetir
            // 
            this.btnTumunuGetir.Location = new System.Drawing.Point(10, 88);
            this.btnTumunuGetir.Size = new System.Drawing.Size(143, 28);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(100, 56);
            this.textBox1.Size = new System.Drawing.Size(140, 22);
            // 
            // btnAra
            // 
            this.btnAra.Location = new System.Drawing.Point(170, 91);
            this.btnAra.Size = new System.Drawing.Size(71, 25);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(7, 59);
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(230, 687);
            this.groupBox2.Visible = false;
            // 
            // txtNo
            // 
            this.txtNo.BackColor = System.Drawing.Color.White;
            this.txtNo.Location = new System.Drawing.Point(359, 26);
            this.txtNo.TextChanged += new System.EventHandler(this.txtNo_TextChanged);
            // 
            // txtSinif
            // 
            this.txtSinif.BackColor = System.Drawing.Color.White;
            this.txtSinif.Location = new System.Drawing.Point(86, 62);
            // 
            // txtAd
            // 
            this.txtAd.BackColor = System.Drawing.Color.White;
            this.txtAd.Location = new System.Drawing.Point(86, 26);
            // 
            // tabControl1
            // 
            this.tabControl1.Location = new System.Drawing.Point(247, 29);
            this.tabControl1.Size = new System.Drawing.Size(616, 318);
            this.tabControl1.Visible = true;
            // 
            // txtVeliYakinligi
            // 
            this.txtVeliYakinligi.Location = new System.Drawing.Point(75, 63);
            // 
            // txtVeliAdSoyad
            // 
            this.txtVeliAdSoyad.Location = new System.Drawing.Point(75, 29);
            // 
            // btnAileKaydet
            // 
            this.btnAileKaydet.Location = new System.Drawing.Point(289, 61);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(866, 79);
            // 
            // txtVeliTel
            // 
            this.txtVeliTel.Location = new System.Drawing.Point(75, 137);
            // 
            // txtVeliMeslek
            // 
            this.txtVeliMeslek.Location = new System.Drawing.Point(75, 103);
            // 
            // txtBabaTel
            // 
            this.txtBabaTel.Location = new System.Drawing.Point(60, 159);
            // 
            // txtBabaMeslek
            // 
            this.txtBabaMeslek.Location = new System.Drawing.Point(60, 130);
            // 
            // txtBabaAd
            // 
            this.txtBabaAd.Location = new System.Drawing.Point(72, 26);
            // 
            // txtAnneAd
            // 
            this.txtAnneAd.Location = new System.Drawing.Point(72, 26);
            // 
            // txtAnneMeslek
            // 
            this.txtAnneMeslek.Location = new System.Drawing.Point(60, 125);
            // 
            // txtAnneTel
            // 
            this.txtAnneTel.Location = new System.Drawing.Point(60, 159);
            // 
            // lblBabaTel
            // 
            this.lblBabaTel.Location = new System.Drawing.Point(11, 162);
            // 
            // lblBabaOz
            // 
            this.lblBabaOz.Location = new System.Drawing.Point(11, 66);
            // 
            // lblBabaMes
            // 
            this.lblBabaMes.Location = new System.Drawing.Point(13, 133);
            // 
            // lblBabaAd
            // 
            this.lblBabaAd.Location = new System.Drawing.Point(2, 30);
            // 
            // grbVeli
            // 
            this.grbVeli.Size = new System.Drawing.Size(187, 254);
            // 
            // grbBaba
            // 
            this.grbBaba.Size = new System.Drawing.Size(198, 254);
            // 
            // lblBabaSag
            // 
            this.lblBabaSag.Location = new System.Drawing.Point(13, 97);
            // 
            // grbAnne
            // 
            this.grbAnne.Size = new System.Drawing.Size(198, 254);
            // 
            // cmbAnneSag
            // 
            this.cmbAnneSag.Location = new System.Drawing.Point(70, 89);
            // 
            // cmbAnneOz
            // 
            this.cmbAnneOz.Location = new System.Drawing.Point(70, 56);
            // 
            // cmAnneBirlik
            // 
            this.cmAnneBirlik.Location = new System.Drawing.Point(96, 219);
            // 
            // lblAnneAd
            // 
            this.lblAnneAd.Location = new System.Drawing.Point(6, 30);
            // 
            // lblAnneBirlik
            // 
            this.lblAnneBirlik.Location = new System.Drawing.Point(9, 222);
            // 
            // lblAnneTel
            // 
            this.lblAnneTel.Location = new System.Drawing.Point(8, 162);
            // 
            // lblAnneMeslek
            // 
            this.lblAnneMeslek.Location = new System.Drawing.Point(6, 128);
            // 
            // lblAnneSag
            // 
            this.lblAnneSag.Location = new System.Drawing.Point(9, 92);
            // 
            // lblAnneOz
            // 
            this.lblAnneOz.Location = new System.Drawing.Point(7, 59);
            // 
            // cmbSagOlu
            // 
            this.cmbSagOlu.Location = new System.Drawing.Point(70, 94);
            // 
            // cmbOzUvey
            // 
            this.cmbOzUvey.Location = new System.Drawing.Point(70, 60);
            // 
            // cmbBirlik
            // 
            this.cmbBirlik.Location = new System.Drawing.Point(93, 216);
            // 
            // lblBirliktelik
            // 
            this.lblBirliktelik.Location = new System.Drawing.Point(11, 219);
            // 
            // richTextAdres
            // 
            this.richTextAdres.Location = new System.Drawing.Point(54, 172);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 314);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(161, 15);
            this.label20.TabIndex = 10;
            this.label20.Text = "Sonuç ve Görüş Öneri Bigileri";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 15);
            this.label18.TabIndex = 12;
            this.label18.Text = "Görüşme Tarihi";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(651, 36);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 15);
            this.label17.TabIndex = 13;
            this.label17.Text = "Görüşmeyi Yapan";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(610, 71);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 15);
            this.label16.TabIndex = 14;
            this.label16.Text = "Sorun Alanları";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(316, 71);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 15);
            this.label15.TabIndex = 15;
            this.label15.Text = "Yapılacak Çalışmalar";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 71);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 15);
            this.label14.TabIndex = 16;
            this.label14.Text = "Görüşme";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.richTextBox1);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.btnOgrenciGorusmeYazdir);
            this.groupBox3.Controls.Add(this.btnGorusmeKaydet);
            this.groupBox3.Controls.Add(this.clistGorusmeSonuc);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.clistYapilacakCalismalar);
            this.groupBox3.Controls.Add(this.clistSorunAlanlari);
            this.groupBox3.Controls.Add(this.dateTimePicker1);
            this.groupBox3.Controls.Add(this.cmbGorusmeyiYapan);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox3.Location = new System.Drawing.Point(37, 370);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(932, 572);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(10, 92);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(292, 217);
            this.richTextBox1.TabIndex = 14;
            this.richTextBox1.Text = "";
            // 
            // btnOgrenciGorusmeYazdir
            // 
            this.btnOgrenciGorusmeYazdir.Enabled = false;
            this.btnOgrenciGorusmeYazdir.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgrenciGorusmeYazdir.Image = global::rbsProje.Properties.Resources.printer;
            this.btnOgrenciGorusmeYazdir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOgrenciGorusmeYazdir.Location = new System.Drawing.Point(440, 22);
            this.btnOgrenciGorusmeYazdir.Name = "btnOgrenciGorusmeYazdir";
            this.btnOgrenciGorusmeYazdir.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnOgrenciGorusmeYazdir.Size = new System.Drawing.Size(101, 40);
            this.btnOgrenciGorusmeYazdir.TabIndex = 12;
            this.btnOgrenciGorusmeYazdir.Text = "           Yazdır";
            this.btnOgrenciGorusmeYazdir.UseVisualStyleBackColor = true;
            this.btnOgrenciGorusmeYazdir.Click += new System.EventHandler(this.btnOgrenciGorusmeYazdir_Click);
            // 
            // btnGorusmeKaydet
            // 
            this.btnGorusmeKaydet.Enabled = false;
            this.btnGorusmeKaydet.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGorusmeKaydet.Image = global::rbsProje.Properties.Resources.diskette2;
            this.btnGorusmeKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGorusmeKaydet.Location = new System.Drawing.Point(322, 22);
            this.btnGorusmeKaydet.Name = "btnGorusmeKaydet";
            this.btnGorusmeKaydet.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnGorusmeKaydet.Size = new System.Drawing.Size(104, 40);
            this.btnGorusmeKaydet.TabIndex = 13;
            this.btnGorusmeKaydet.Text = "         Kaydet";
            this.btnGorusmeKaydet.UseVisualStyleBackColor = true;
            this.btnGorusmeKaydet.Click += new System.EventHandler(this.btnGorusmeKaydet_Click);
            // 
            // clistGorusmeSonuc
            // 
            this.clistGorusmeSonuc.CheckOnClick = true;
            this.clistGorusmeSonuc.FormattingEnabled = true;
            this.clistGorusmeSonuc.HorizontalScrollbar = true;
            this.clistGorusmeSonuc.Location = new System.Drawing.Point(6, 333);
            this.clistGorusmeSonuc.Name = "clistGorusmeSonuc";
            this.clistGorusmeSonuc.Size = new System.Drawing.Size(916, 238);
            this.clistGorusmeSonuc.TabIndex = 3;
            // 
            // clistYapilacakCalismalar
            // 
            this.clistYapilacakCalismalar.CheckOnClick = true;
            this.clistYapilacakCalismalar.FormattingEnabled = true;
            this.clistYapilacakCalismalar.HorizontalScrollbar = true;
            this.clistYapilacakCalismalar.Location = new System.Drawing.Point(319, 91);
            this.clistYapilacakCalismalar.Name = "clistYapilacakCalismalar";
            this.clistYapilacakCalismalar.Size = new System.Drawing.Size(279, 220);
            this.clistYapilacakCalismalar.TabIndex = 3;
            // 
            // clistSorunAlanlari
            // 
            this.clistSorunAlanlari.CheckOnClick = true;
            this.clistSorunAlanlari.FormattingEnabled = true;
            this.clistSorunAlanlari.HorizontalScrollbar = true;
            this.clistSorunAlanlari.Location = new System.Drawing.Point(613, 91);
            this.clistSorunAlanlari.Name = "clistSorunAlanlari";
            this.clistSorunAlanlari.Size = new System.Drawing.Size(309, 220);
            this.clistSorunAlanlari.TabIndex = 3;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "";
            this.dateTimePicker1.Location = new System.Drawing.Point(107, 31);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(195, 23);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // cmbGorusmeyiYapan
            // 
            this.cmbGorusmeyiYapan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGorusmeyiYapan.FormattingEnabled = true;
            this.cmbGorusmeyiYapan.Location = new System.Drawing.Point(756, 33);
            this.cmbGorusmeyiYapan.Name = "cmbGorusmeyiYapan";
            this.cmbGorusmeyiYapan.Size = new System.Drawing.Size(159, 23);
            this.cmbGorusmeyiYapan.TabIndex = 1;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label29.Location = new System.Drawing.Point(11, 10);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(106, 21);
            this.label29.TabIndex = 12;
            this.label29.Text = "Veli Görüşme";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // lblVeliGorusmeBaslik
            // 
            this.lblVeliGorusmeBaslik.AutoSize = true;
            this.lblVeliGorusmeBaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblVeliGorusmeBaslik.Location = new System.Drawing.Point(395, -1);
            this.lblVeliGorusmeBaslik.Name = "lblVeliGorusmeBaslik";
            this.lblVeliGorusmeBaslik.Size = new System.Drawing.Size(125, 25);
            this.lblVeliGorusmeBaslik.TabIndex = 11;
            this.lblVeliGorusmeBaslik.Text = "Veli Görüşme";
            // 
            // UCveliGorusme
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Name = "UCveliGorusme";
            this.Size = new System.Drawing.Size(1023, 946);
            this.Load += new System.EventHandler(this.UCveliGorusme_Load);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.lblListeleBaslik, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grbVeli.ResumeLayout(false);
            this.grbVeli.PerformLayout();
            this.grbBaba.ResumeLayout(false);
            this.grbBaba.PerformLayout();
            this.grbAnne.ResumeLayout(false);
            this.grbAnne.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btnOgrenciGorusmeYazdir;
        private System.Windows.Forms.CheckedListBox clistGorusmeSonuc;
        private System.Windows.Forms.CheckedListBox clistYapilacakCalismalar;
        private System.Windows.Forms.CheckedListBox clistSorunAlanlari;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox cmbGorusmeyiYapan;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label lblVeliGorusmeBaslik;
        public System.Windows.Forms.Button btnGorusmeKaydet;
    }
}
