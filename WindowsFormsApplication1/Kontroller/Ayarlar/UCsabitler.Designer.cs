﻿namespace rbsProje.Kontroller.Ayarlar
{
    partial class UCsabitler
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCsabitler));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblUyari1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblOgrenciBaslik = new System.Windows.Forms.Label();
            this.lblSecim = new System.Windows.Forms.Label();
            this.btnOgrenciKaynakGetir = new System.Windows.Forms.Button();
            this.cmbOgrenci = new System.Windows.Forms.ComboBox();
            this.gridOgrenci = new System.Windows.Forms.DataGridView();
            this.navOgrenci = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lblUyari2 = new System.Windows.Forms.Label();
            this.navVeli = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbVeli = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnVeli = new System.Windows.Forms.Button();
            this.gridVeli = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblUyari3 = new System.Windows.Forms.Label();
            this.navOgretmen = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbOgretmen = new System.Windows.Forms.ComboBox();
            this.lblOgretmenBaslik = new System.Windows.Forms.Label();
            this.lblSecim2 = new System.Windows.Forms.Label();
            this.btnOgretmen = new System.Windows.Forms.Button();
            this.gridOgretmen = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.navRehberci = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox3 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.lblUyari4 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnRehberci = new System.Windows.Forms.Button();
            this.gridRehberci = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSinifRehberGetir = new System.Windows.Forms.Button();
            this.txtSinifRehber = new System.Windows.Forms.TextBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.btnSinifRehberKaydet = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridOgrenci)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navOgrenci)).BeginInit();
            this.navOgrenci.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navVeli)).BeginInit();
            this.navVeli.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridVeli)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navOgretmen)).BeginInit();
            this.navOgretmen.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridOgretmen)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navRehberci)).BeginInit();
            this.navRehberci.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRehberci)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(704, 465);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Silver;
            this.tabPage1.Controls.Add(this.lblUyari1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.gridOgrenci);
            this.tabPage1.Controls.Add(this.navOgrenci);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(696, 439);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Öğrenci Görüşme Sabitleri";
            // 
            // lblUyari1
            // 
            this.lblUyari1.AutoSize = true;
            this.lblUyari1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUyari1.Location = new System.Drawing.Point(517, 172);
            this.lblUyari1.Name = "lblUyari1";
            this.lblUyari1.Size = new System.Drawing.Size(144, 30);
            this.lblUyari1.TabIndex = 6;
            this.lblUyari1.Text = "Değişiklikleri kaydetmeyi \r\n         unutmayınız.";
            this.lblUyari1.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblOgrenciBaslik);
            this.groupBox1.Controls.Add(this.lblSecim);
            this.groupBox1.Controls.Add(this.btnOgrenciKaynakGetir);
            this.groupBox1.Controls.Add(this.cmbOgrenci);
            this.groupBox1.Location = new System.Drawing.Point(43, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(618, 119);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // lblOgrenciBaslik
            // 
            this.lblOgrenciBaslik.AutoSize = true;
            this.lblOgrenciBaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblOgrenciBaslik.Location = new System.Drawing.Point(192, -2);
            this.lblOgrenciBaslik.Name = "lblOgrenciBaslik";
            this.lblOgrenciBaslik.Size = new System.Drawing.Size(202, 21);
            this.lblOgrenciBaslik.TabIndex = 4;
            this.lblOgrenciBaslik.Text = "Öğrenci Görüşme Sabitleri";
            // 
            // lblSecim
            // 
            this.lblSecim.AutoSize = true;
            this.lblSecim.Location = new System.Drawing.Point(67, 51);
            this.lblSecim.Name = "lblSecim";
            this.lblSecim.Size = new System.Drawing.Size(73, 13);
            this.lblSecim.TabIndex = 4;
            this.lblSecim.Text = "Seçim Yapınız";
            // 
            // btnOgrenciKaynakGetir
            // 
            this.btnOgrenciKaynakGetir.Image = global::rbsProje.Properties.Resources.list1;
            this.btnOgrenciKaynakGetir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOgrenciKaynakGetir.Location = new System.Drawing.Point(388, 39);
            this.btnOgrenciKaynakGetir.Name = "btnOgrenciKaynakGetir";
            this.btnOgrenciKaynakGetir.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.btnOgrenciKaynakGetir.Size = new System.Drawing.Size(119, 50);
            this.btnOgrenciKaynakGetir.TabIndex = 3;
            this.btnOgrenciKaynakGetir.Text = "    Getir";
            this.btnOgrenciKaynakGetir.UseVisualStyleBackColor = true;
            this.btnOgrenciKaynakGetir.Click += new System.EventHandler(this.btnOgrenciKaynakGetir_Click);
            // 
            // cmbOgrenci
            // 
            this.cmbOgrenci.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOgrenci.FormattingEnabled = true;
            this.cmbOgrenci.Items.AddRange(new object[] {
            "Görüşme Türleri",
            "PDR Alanları",
            "Sonuç Cümleleri",
            "Sorun Alanları",
            "Yapılacak Çalışmalar",
            "Yönlendirilen Kurumlar"});
            this.cmbOgrenci.Location = new System.Drawing.Point(160, 48);
            this.cmbOgrenci.Name = "cmbOgrenci";
            this.cmbOgrenci.Size = new System.Drawing.Size(151, 21);
            this.cmbOgrenci.TabIndex = 2;
            // 
            // gridOgrenci
            // 
            this.gridOgrenci.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridOgrenci.ColumnHeadersVisible = false;
            this.gridOgrenci.Location = new System.Drawing.Point(43, 215);
            this.gridOgrenci.Name = "gridOgrenci";
            this.gridOgrenci.RowHeadersVisible = false;
            this.gridOgrenci.Size = new System.Drawing.Size(618, 203);
            this.gridOgrenci.TabIndex = 4;
            this.gridOgrenci.Visible = false;
            // 
            // navOgrenci
            // 
            this.navOgrenci.AddNewItem = this.bindingNavigatorAddNewItem;
            this.navOgrenci.CountItem = this.bindingNavigatorCountItem;
            this.navOgrenci.DeleteItem = this.bindingNavigatorDeleteItem;
            this.navOgrenci.Dock = System.Windows.Forms.DockStyle.None;
            this.navOgrenci.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.navOgrenci.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.saveToolStripButton,
            this.toolStripSeparator1});
            this.navOgrenci.Location = new System.Drawing.Point(247, 162);
            this.navOgrenci.MoveFirstItem = null;
            this.navOgrenci.MoveLastItem = null;
            this.navOgrenci.MoveNextItem = null;
            this.navOgrenci.MovePreviousItem = null;
            this.navOgrenci.Name = "navOgrenci";
            this.navOgrenci.PositionItem = this.bindingNavigatorPositionItem;
            this.navOgrenci.Size = new System.Drawing.Size(177, 25);
            this.navOgrenci.TabIndex = 1;
            this.navOgrenci.Text = "bindingNavigator1";
            this.navOgrenci.Visible = false;
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Ekle";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Sil";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "&Kaydet";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Silver;
            this.tabPage3.Controls.Add(this.lblUyari2);
            this.tabPage3.Controls.Add(this.navVeli);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.gridVeli);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(696, 439);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Veli Görüşme Sabitleri";
            // 
            // lblUyari2
            // 
            this.lblUyari2.AutoSize = true;
            this.lblUyari2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUyari2.Location = new System.Drawing.Point(512, 170);
            this.lblUyari2.Name = "lblUyari2";
            this.lblUyari2.Size = new System.Drawing.Size(144, 30);
            this.lblUyari2.TabIndex = 12;
            this.lblUyari2.Text = "Değişiklikleri kaydetmeyi \r\n         unutmayınız.";
            this.lblUyari2.Visible = false;
            // 
            // navVeli
            // 
            this.navVeli.AddNewItem = this.toolStripButton4;
            this.navVeli.CountItem = this.toolStripLabel2;
            this.navVeli.DeleteItem = this.toolStripButton5;
            this.navVeli.Dock = System.Windows.Forms.DockStyle.None;
            this.navVeli.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.navVeli.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator5,
            this.toolStripTextBox2,
            this.toolStripLabel2,
            this.toolStripSeparator6,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripSeparator7});
            this.navVeli.Location = new System.Drawing.Point(217, 167);
            this.navVeli.MoveFirstItem = null;
            this.navVeli.MoveLastItem = null;
            this.navVeli.MoveNextItem = null;
            this.navVeli.MovePreviousItem = null;
            this.navVeli.Name = "navVeli";
            this.navVeli.PositionItem = this.toolStripTextBox2;
            this.navVeli.Size = new System.Drawing.Size(177, 25);
            this.navVeli.TabIndex = 11;
            this.navVeli.Text = "bindingNavigator1";
            this.navVeli.Visible = false;
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.RightToLeftAutoMirrorImage = true;
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "Ekle";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel2.Text = "of {0}";
            this.toolStripLabel2.ToolTipText = "Total number of items";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.RightToLeftAutoMirrorImage = true;
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "Sil";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.AccessibleName = "Position";
            this.toolStripTextBox2.AutoSize = false;
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox2.Text = "0";
            this.toolStripTextBox2.ToolTipText = "Current position";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "&Kaydet";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbVeli);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.btnVeli);
            this.groupBox3.Location = new System.Drawing.Point(38, 22);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(618, 119);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            // 
            // cmbVeli
            // 
            this.cmbVeli.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVeli.FormattingEnabled = true;
            this.cmbVeli.Items.AddRange(new object[] {
            "Sonuç Cümleleri",
            "Sorun Alanları",
            "Yapılacak Çalışmalar"});
            this.cmbVeli.Location = new System.Drawing.Point(160, 48);
            this.cmbVeli.Name = "cmbVeli";
            this.cmbVeli.Size = new System.Drawing.Size(151, 21);
            this.cmbVeli.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(192, -2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 21);
            this.label1.TabIndex = 4;
            this.label1.Text = "Veli Görüşme Sabitleri";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Seçim Yapınız";
            // 
            // btnVeli
            // 
            this.btnVeli.Image = global::rbsProje.Properties.Resources.list1;
            this.btnVeli.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVeli.Location = new System.Drawing.Point(388, 39);
            this.btnVeli.Name = "btnVeli";
            this.btnVeli.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.btnVeli.Size = new System.Drawing.Size(119, 50);
            this.btnVeli.TabIndex = 3;
            this.btnVeli.Text = "    Getir";
            this.btnVeli.UseVisualStyleBackColor = true;
            this.btnVeli.Click += new System.EventHandler(this.btnVeli_Click);
            // 
            // gridVeli
            // 
            this.gridVeli.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridVeli.ColumnHeadersVisible = false;
            this.gridVeli.Location = new System.Drawing.Point(38, 214);
            this.gridVeli.Name = "gridVeli";
            this.gridVeli.RowHeadersVisible = false;
            this.gridVeli.Size = new System.Drawing.Size(618, 203);
            this.gridVeli.TabIndex = 9;
            this.gridVeli.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Silver;
            this.tabPage2.Controls.Add(this.lblUyari3);
            this.tabPage2.Controls.Add(this.navOgretmen);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.gridOgretmen);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(696, 439);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Öğretmen Görüşme Sabitleri";
            // 
            // lblUyari3
            // 
            this.lblUyari3.AutoSize = true;
            this.lblUyari3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUyari3.Location = new System.Drawing.Point(513, 172);
            this.lblUyari3.Name = "lblUyari3";
            this.lblUyari3.Size = new System.Drawing.Size(144, 30);
            this.lblUyari3.TabIndex = 9;
            this.lblUyari3.Text = "Değişiklikleri kaydetmeyi \r\n         unutmayınız.";
            this.lblUyari3.Visible = false;
            // 
            // navOgretmen
            // 
            this.navOgretmen.AddNewItem = this.toolStripButton1;
            this.navOgretmen.CountItem = this.toolStripLabel1;
            this.navOgretmen.DeleteItem = this.toolStripButton2;
            this.navOgretmen.Dock = System.Windows.Forms.DockStyle.None;
            this.navOgretmen.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.navOgretmen.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator2,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator3,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripSeparator4});
            this.navOgretmen.Location = new System.Drawing.Point(242, 161);
            this.navOgretmen.MoveFirstItem = null;
            this.navOgretmen.MoveLastItem = null;
            this.navOgretmen.MoveNextItem = null;
            this.navOgretmen.MovePreviousItem = null;
            this.navOgretmen.Name = "navOgretmen";
            this.navOgretmen.PositionItem = this.toolStripTextBox1;
            this.navOgretmen.Size = new System.Drawing.Size(177, 25);
            this.navOgretmen.TabIndex = 6;
            this.navOgretmen.Text = "bindingNavigator1";
            this.navOgretmen.Visible = false;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Ekle";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel1.Text = "of {0}";
            this.toolStripLabel1.ToolTipText = "Total number of items";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.RightToLeftAutoMirrorImage = true;
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Sil";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "Position";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "Current position";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "&Kaydet";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbOgretmen);
            this.groupBox2.Controls.Add(this.lblOgretmenBaslik);
            this.groupBox2.Controls.Add(this.lblSecim2);
            this.groupBox2.Controls.Add(this.btnOgretmen);
            this.groupBox2.Location = new System.Drawing.Point(39, 22);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(618, 119);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            // 
            // cmbOgretmen
            // 
            this.cmbOgretmen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOgretmen.FormattingEnabled = true;
            this.cmbOgretmen.Items.AddRange(new object[] {
            "Sonuç Cümleleri",
            "Sorun Alanları",
            "Yapılacak Çalışmalar"});
            this.cmbOgretmen.Location = new System.Drawing.Point(160, 48);
            this.cmbOgretmen.Name = "cmbOgretmen";
            this.cmbOgretmen.Size = new System.Drawing.Size(151, 21);
            this.cmbOgretmen.TabIndex = 2;
            // 
            // lblOgretmenBaslik
            // 
            this.lblOgretmenBaslik.AutoSize = true;
            this.lblOgretmenBaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblOgretmenBaslik.Location = new System.Drawing.Point(192, -2);
            this.lblOgretmenBaslik.Name = "lblOgretmenBaslik";
            this.lblOgretmenBaslik.Size = new System.Drawing.Size(219, 21);
            this.lblOgretmenBaslik.TabIndex = 4;
            this.lblOgretmenBaslik.Text = "Öğretmen Görüşme Sabitleri";
            // 
            // lblSecim2
            // 
            this.lblSecim2.AutoSize = true;
            this.lblSecim2.Location = new System.Drawing.Point(67, 51);
            this.lblSecim2.Name = "lblSecim2";
            this.lblSecim2.Size = new System.Drawing.Size(73, 13);
            this.lblSecim2.TabIndex = 4;
            this.lblSecim2.Text = "Seçim Yapınız";
            // 
            // btnOgretmen
            // 
            this.btnOgretmen.Image = global::rbsProje.Properties.Resources.list1;
            this.btnOgretmen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOgretmen.Location = new System.Drawing.Point(388, 39);
            this.btnOgretmen.Name = "btnOgretmen";
            this.btnOgretmen.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.btnOgretmen.Size = new System.Drawing.Size(119, 50);
            this.btnOgretmen.TabIndex = 3;
            this.btnOgretmen.Text = "    Getir";
            this.btnOgretmen.UseVisualStyleBackColor = true;
            this.btnOgretmen.Click += new System.EventHandler(this.btnOgretmen_Click);
            // 
            // gridOgretmen
            // 
            this.gridOgretmen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridOgretmen.ColumnHeadersVisible = false;
            this.gridOgretmen.Location = new System.Drawing.Point(39, 214);
            this.gridOgretmen.Name = "gridOgretmen";
            this.gridOgretmen.RowHeadersVisible = false;
            this.gridOgretmen.Size = new System.Drawing.Size(618, 203);
            this.gridOgretmen.TabIndex = 7;
            this.gridOgretmen.Visible = false;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Silver;
            this.tabPage4.Controls.Add(this.navRehberci);
            this.tabPage4.Controls.Add(this.lblUyari4);
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Controls.Add(this.gridRehberci);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(696, 439);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Rehber Öğretmenler";
            // 
            // navRehberci
            // 
            this.navRehberci.AddNewItem = this.toolStripButton7;
            this.navRehberci.CountItem = this.toolStripLabel3;
            this.navRehberci.DeleteItem = this.toolStripButton8;
            this.navRehberci.Dock = System.Windows.Forms.DockStyle.None;
            this.navRehberci.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.navRehberci.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator8,
            this.toolStripTextBox3,
            this.toolStripLabel3,
            this.toolStripSeparator9,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton9,
            this.toolStripSeparator10});
            this.navRehberci.Location = new System.Drawing.Point(219, 165);
            this.navRehberci.MoveFirstItem = null;
            this.navRehberci.MoveLastItem = null;
            this.navRehberci.MoveNextItem = null;
            this.navRehberci.MovePreviousItem = null;
            this.navRehberci.Name = "navRehberci";
            this.navRehberci.PositionItem = this.toolStripTextBox3;
            this.navRehberci.Size = new System.Drawing.Size(177, 25);
            this.navRehberci.TabIndex = 13;
            this.navRehberci.Text = "bindingNavigator1";
            this.navRehberci.Visible = false;
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.RightToLeftAutoMirrorImage = true;
            this.toolStripButton7.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton7.Text = "Ekle";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel3.Text = "of {0}";
            this.toolStripLabel3.ToolTipText = "Total number of items";
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.RightToLeftAutoMirrorImage = true;
            this.toolStripButton8.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton8.Text = "Sil";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox3
            // 
            this.toolStripTextBox3.AccessibleName = "Position";
            this.toolStripTextBox3.AutoSize = false;
            this.toolStripTextBox3.Name = "toolStripTextBox3";
            this.toolStripTextBox3.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox3.Text = "0";
            this.toolStripTextBox3.ToolTipText = "Current position";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton9.Text = "&Kaydet";
            this.toolStripButton9.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 25);
            // 
            // lblUyari4
            // 
            this.lblUyari4.AutoSize = true;
            this.lblUyari4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUyari4.Location = new System.Drawing.Point(513, 170);
            this.lblUyari4.Name = "lblUyari4";
            this.lblUyari4.Size = new System.Drawing.Size(144, 30);
            this.lblUyari4.TabIndex = 12;
            this.lblUyari4.Text = "Değişiklikleri kaydetmeyi \r\n         unutmayınız.";
            this.lblUyari4.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.btnRehberci);
            this.groupBox4.Location = new System.Drawing.Point(39, 22);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(618, 119);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(192, -2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 21);
            this.label4.TabIndex = 4;
            this.label4.Text = "Rehber Öğretmenler";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(53, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(210, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Rehber öğretmenleri listeleyiniz.";
            // 
            // btnRehberci
            // 
            this.btnRehberci.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnRehberci.Image = global::rbsProje.Properties.Resources.uye1;
            this.btnRehberci.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRehberci.Location = new System.Drawing.Point(282, 32);
            this.btnRehberci.Name = "btnRehberci";
            this.btnRehberci.Padding = new System.Windows.Forms.Padding(9, 0, 0, 0);
            this.btnRehberci.Size = new System.Drawing.Size(174, 50);
            this.btnRehberci.TabIndex = 3;
            this.btnRehberci.Text = "          Rehber Öğretmenleri\r\n    Getir";
            this.btnRehberci.UseVisualStyleBackColor = true;
            this.btnRehberci.Click += new System.EventHandler(this.btnRehberci_Click);
            // 
            // gridRehberci
            // 
            this.gridRehberci.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRehberci.ColumnHeadersVisible = false;
            this.gridRehberci.Location = new System.Drawing.Point(39, 214);
            this.gridRehberci.Name = "gridRehberci";
            this.gridRehberci.RowHeadersVisible = false;
            this.gridRehberci.Size = new System.Drawing.Size(618, 203);
            this.gridRehberci.TabIndex = 10;
            this.gridRehberci.Visible = false;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.Silver;
            this.tabPage5.Controls.Add(this.groupBox5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(696, 439);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Sınıf Rehber Öğretmen";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.treeView1);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.txtSinifRehber);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.btnSinifRehberKaydet);
            this.groupBox5.Controls.Add(this.btnSinifRehberGetir);
            this.groupBox5.Location = new System.Drawing.Point(37, 50);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(618, 339);
            this.groupBox5.TabIndex = 12;
            this.groupBox5.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(192, -2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(197, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "Sınıf Rehber Öğretmenler";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(144, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(221, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "Sınıf Rehber öğretmeni adı soyadı";
            // 
            // btnSinifRehberGetir
            // 
            this.btnSinifRehberGetir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSinifRehberGetir.Image = global::rbsProje.Properties.Resources.uye1;
            this.btnSinifRehberGetir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSinifRehberGetir.Location = new System.Drawing.Point(334, 34);
            this.btnSinifRehberGetir.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.btnSinifRehberGetir.Name = "btnSinifRehberGetir";
            this.btnSinifRehberGetir.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnSinifRehberGetir.Size = new System.Drawing.Size(107, 57);
            this.btnSinifRehberGetir.TabIndex = 3;
            this.btnSinifRehberGetir.Text = "        Getir\r\n";
            this.btnSinifRehberGetir.UseVisualStyleBackColor = true;
            this.btnSinifRehberGetir.Click += new System.EventHandler(this.btnSinifRehberGetir_Click);
            // 
            // txtSinifRehber
            // 
            this.txtSinifRehber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtSinifRehber.Location = new System.Drawing.Point(371, 148);
            this.txtSinifRehber.Name = "txtSinifRehber";
            this.txtSinifRehber.Size = new System.Drawing.Size(153, 22);
            this.txtSinifRehber.TabIndex = 14;
            // 
            // treeView1
            // 
            this.treeView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.treeView1.Location = new System.Drawing.Point(44, 112);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(82, 206);
            this.treeView1.TabIndex = 15;
            // 
            // btnSinifRehberKaydet
            // 
            this.btnSinifRehberKaydet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSinifRehberKaydet.Image = global::rbsProje.Properties.Resources.kaydet3;
            this.btnSinifRehberKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSinifRehberKaydet.Location = new System.Drawing.Point(306, 201);
            this.btnSinifRehberKaydet.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.btnSinifRehberKaydet.Name = "btnSinifRehberKaydet";
            this.btnSinifRehberKaydet.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnSinifRehberKaydet.Size = new System.Drawing.Size(107, 57);
            this.btnSinifRehberKaydet.TabIndex = 3;
            this.btnSinifRehberKaydet.Text = "        Kaydet";
            this.btnSinifRehberKaydet.UseVisualStyleBackColor = true;
            this.btnSinifRehberKaydet.Click += new System.EventHandler(this.btnSinifRehberKaydet_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.Location = new System.Drawing.Point(167, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "Sınıfları listeleyiniz";
            // 
            // UCsabitler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "UCsabitler";
            this.Size = new System.Drawing.Size(720, 480);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridOgrenci)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navOgrenci)).EndInit();
            this.navOgrenci.ResumeLayout(false);
            this.navOgrenci.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navVeli)).EndInit();
            this.navVeli.ResumeLayout(false);
            this.navVeli.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridVeli)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navOgretmen)).EndInit();
            this.navOgretmen.ResumeLayout(false);
            this.navOgretmen.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridOgretmen)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navRehberci)).EndInit();
            this.navRehberci.ResumeLayout(false);
            this.navRehberci.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRehberci)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView gridOgrenci;
        private System.Windows.Forms.Button btnOgrenciKaynakGetir;
        private System.Windows.Forms.ComboBox cmbOgrenci;
        private System.Windows.Forms.BindingNavigator navOgrenci;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblSecim;
        private System.Windows.Forms.Label lblOgrenciBaslik;
        private System.Windows.Forms.BindingNavigator navOgretmen;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblOgretmenBaslik;
        private System.Windows.Forms.Label lblSecim2;
        private System.Windows.Forms.Button btnOgretmen;
        private System.Windows.Forms.ComboBox cmbOgretmen;
        private System.Windows.Forms.DataGridView gridOgretmen;
        private System.Windows.Forms.BindingNavigator navVeli;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cmbVeli;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnVeli;
        private System.Windows.Forms.DataGridView gridVeli;
        private System.Windows.Forms.Label lblUyari1;
        private System.Windows.Forms.Label lblUyari2;
        private System.Windows.Forms.Label lblUyari3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.BindingNavigator navRehberci;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.Label lblUyari4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnRehberci;
        private System.Windows.Forms.DataGridView gridRehberci;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSinifRehberGetir;
        private System.Windows.Forms.TextBox txtSinifRehber;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button btnSinifRehberKaydet;
        private System.Windows.Forms.Label label7;
    }
}
