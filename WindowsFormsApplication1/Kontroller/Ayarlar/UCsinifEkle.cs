﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;

namespace rbsProje.Kontroller.Ayarlar
{
    public partial class UCsinifEkle : UserControl
    {
        public UCsinifEkle()
        {
            InitializeComponent();
        }

        SQLiteDataAdapter adapter;
        DataSet ds;


        private void UCsinifEkle_Load(object sender, EventArgs e)
        {
            string komut = "SELECT * FROM SINIFLAR";
            var DBbaglanti = new SQLiteConnection("Data Source=db.sqlite;Version=3;");
            SQLiteCommand command = new SQLiteCommand(komut, DBbaglanti);
            DBbaglanti.Open();
            adapter = new SQLiteDataAdapter(command);
            ds = new DataSet();
            try
            {
                adapter.Fill(ds);
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Veritabanı hatası.", "Sınıf Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DBbaglanti.Close();
            SQLiteCommandBuilder cb = new SQLiteCommandBuilder(adapter);
            cb.ConflictOption = ConflictOption.OverwriteChanges;

            BindingSource sc = new BindingSource();
          
                sc.DataSource = ds.Tables[0];
            

            bindingNavigator1.BindingSource = sc;
            dataGridView1.DataSource = sc;
        }


        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.dataGridView1.EndEdit();
                adapter.Update(ds);
                MessageBox.Show("İşlem başarılı.", "Sınıf Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception)
            {
                MessageBox.Show("Hata. Alanları kontrol ediniz. Sınıfların aynı isimli olmaması gerekmektedir.", "Sınıf Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.BeginEdit(true);
        }


    }
}
