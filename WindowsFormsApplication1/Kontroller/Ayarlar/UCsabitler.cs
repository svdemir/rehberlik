﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Linq;

namespace rbsProje.Kontroller.Ayarlar
{
    public partial class UCsabitler : UserControl
    {
        public UCsabitler()
        {
            InitializeComponent();
        }


        SQLiteDataAdapter adapter;
        DataSet ds;
        SQLiteConnection DBbaglanti = new SQLiteConnection("Data Source=db.sqlite;Version=3;");


        private void btnOgrenciKaynakGetir_Click(object sender, EventArgs e)
        {
            string komut = "";

            if (cmbOgrenci.SelectedIndex == -1)
            {
                MessageBox.Show("Seçim yapınız.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (cmbOgrenci.SelectedIndex == 0)
            {
                komut = "SELECT * FROM kaynakGorusmeTuru";
            }
            else if (cmbOgrenci.SelectedIndex == 1)
            {
                komut = "SELECT * FROM kaynakPdrAlanlari";
            }
            else if (cmbOgrenci.SelectedIndex == 2)
            {
                komut = "SELECT * FROM kaynakSonucCumleleri";
            }
            else if (cmbOgrenci.SelectedIndex == 3)
            {
                komut = "SELECT * FROM kaynakSorunAlanlari";
            }
            else if (cmbOgrenci.SelectedIndex == 4)
            {
                komut = "SELECT * FROM kaynakYapilacakCalismalar";
            }
            else if (cmbOgrenci.SelectedIndex == 5)
            {
                komut = "SELECT * FROM kaynakYonlendirilenKurum";
            }


            SQLiteCommand command = new SQLiteCommand(komut, DBbaglanti);
            DBbaglanti.Open();
            adapter = new SQLiteDataAdapter(command);
            ds = new DataSet();
            try
            {
                adapter.Fill(ds);
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Veritabanı hatası.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DBbaglanti.Close();
            SQLiteCommandBuilder cb = new SQLiteCommandBuilder(adapter);
            cb.ConflictOption = ConflictOption.OverwriteChanges;
            BindingSource sc = new BindingSource();
            sc.DataSource = ds.Tables[0];
            navOgrenci.BindingSource = sc;
            gridOgrenci.DataSource = sc;
            gridOgrenci.Columns[0].Visible = false;
            gridOgrenci.Columns[1].Width = 550;
            gridOgrenci.Visible = true;
            navOgrenci.Visible = true;
            lblUyari1.Visible = true;
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                adapter.Update(ds);
                MessageBox.Show("İşlem başarılı.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnOgrenciKaynakGetir.PerformClick();
            }
            catch (Exception)
            {
                MessageBox.Show("Hata. Alanları kontrol ediniz.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnOgretmen_Click(object sender, EventArgs e)
        {
            string komut = "";

            if (cmbOgretmen.SelectedIndex == -1)
            {
                MessageBox.Show("Seçim yapınız.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (cmbOgretmen.SelectedIndex == 0)
            {
                komut = "SELECT * FROM kaynakOgretmenSonucCumleleri";
            }
            else if (cmbOgretmen.SelectedIndex == 1)
            {
                komut = "SELECT * FROM kaynakOgretmenSorunAlanlari";
            }
            else if (cmbOgretmen.SelectedIndex == 2)
            {
                komut = "SELECT * FROM kaynakOgretmenYapilacakCalismalar";
            }

            SQLiteCommand command = new SQLiteCommand(komut, DBbaglanti);
            DBbaglanti.Open();
            adapter = new SQLiteDataAdapter(command);

            ds = new DataSet();
            try
            {
                adapter.Fill(ds);
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Veritabanı hatası.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DBbaglanti.Close();
            SQLiteCommandBuilder cb = new SQLiteCommandBuilder(adapter);
            cb.ConflictOption = ConflictOption.OverwriteChanges;
            BindingSource sc = new BindingSource();
            sc.DataSource = ds.Tables[0];
            navOgretmen.BindingSource = sc;
            gridOgretmen.DataSource = sc;
            gridOgretmen.Columns[0].Visible = false;
            gridOgretmen.Columns[1].Width = 550;
            gridOgretmen.Visible = true;
            navOgretmen.Visible = true;
            lblUyari3.Visible = true;

        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                adapter.Update(ds);
                MessageBox.Show("İşlem başarılı.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnOgretmen.PerformClick();
            }
            catch (Exception)
            {
                MessageBox.Show("Hata. Alanları kontrol ediniz.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnVeli_Click(object sender, EventArgs e)
        {
            string komut = "";

            if (cmbVeli.SelectedIndex == -1)
            {
                MessageBox.Show("Seçim yapınız.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (cmbVeli.SelectedIndex == 0)
            {
                komut = "SELECT * FROM kaynakVeliSonucCumleleri";
            }
            else if (cmbVeli.SelectedIndex == 1)
            {
                komut = "SELECT * FROM kaynakVeliSorunAlanlari";
            }
            else if (cmbVeli.SelectedIndex == 2)
            {
                komut = "SELECT * FROM kaynakVeliYapilacakCalismalar";
            }

            SQLiteCommand command = new SQLiteCommand(komut, DBbaglanti);
            DBbaglanti.Open();
            adapter = new SQLiteDataAdapter(command);

            ds = new DataSet();
            try
            {
                adapter.Fill(ds);
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Veritabanı hatası.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DBbaglanti.Close();
            SQLiteCommandBuilder cb = new SQLiteCommandBuilder(adapter);
            cb.ConflictOption = ConflictOption.OverwriteChanges;
            BindingSource sc = new BindingSource();
            sc.DataSource = ds.Tables[0];
            navVeli.BindingSource = sc;
            gridVeli.DataSource = sc;
            gridVeli.Columns[0].Visible = false;
            gridVeli.Columns[1].Width = 550;
            gridVeli.Visible = true;
            navVeli.Visible = true;
            lblUyari2.Visible = true;

        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                adapter.Update(ds);
                MessageBox.Show("İşlem başarılı.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnVeli.PerformClick();
            }
            catch (Exception)
            {
                MessageBox.Show("Hata. Alanları kontrol ediniz.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRehberci_Click(object sender, EventArgs e)
        {
            string komut = "SELECT * FROM rehberOgretmenler";

            SQLiteCommand command = new SQLiteCommand(komut, DBbaglanti);
            DBbaglanti.Open();
            adapter = new SQLiteDataAdapter(command);
            ds = new DataSet();
            try
            {
                adapter.Fill(ds);
            }
            catch (Exception)
            {
                MessageBox.Show("İşlem gerçekleştirilemedi. Veritabanı hatası.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DBbaglanti.Close();
            SQLiteCommandBuilder cb = new SQLiteCommandBuilder(adapter);
            cb.ConflictOption = ConflictOption.OverwriteChanges;
            BindingSource sc = new BindingSource();
            sc.DataSource = ds.Tables[0];
            navRehberci.BindingSource = sc;
            gridRehberci.DataSource = sc;
            gridRehberci.Columns[0].Visible = false;
            gridRehberci.Columns[1].Width = 550;
            gridRehberci.Visible = true;
            navRehberci.Visible = true;
            lblUyari4.Visible = true;
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                adapter.Update(ds);
                MessageBox.Show("İşlem başarılı.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnRehberci.PerformClick();
            }
            catch (Exception)
            {
                MessageBox.Show("Hata. Alanları kontrol ediniz.", "Sabit Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSinifRehberGetir_Click(object sender, EventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var liste = veritabanim.UYELER.GroupBy(p => p.SINIF).ToList();
                treeView1.Nodes.Clear();
                if (liste.Count > 0)
                {
                    foreach (var item in liste)
                    {
                        treeView1.Nodes.Add(item.Key);
                    }
                }
                else
                {
                    treeView1.Nodes.Add("Yok.");
                }
            }
        }

        private void btnSinifRehberKaydet_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode==null)
            {
                MessageBox.Show("Sınıf seçiniz.", "Sınıf Rehber Öğretmeni Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var sinifListe = veritabanim.UYELER.Where(p => p.SINIF == treeView1.SelectedNode.Text).ToList();
                    for (int i = 0; i < sinifListe.Count; i++)
                    {
                        sinifListe[i].sinifRehberOgrt = txtSinifRehber.Text;
                    }
                    veritabanim.SaveChanges();
                    MessageBox.Show("İşlem başarılı.", "Sınıf Rehber Öğretmeni Düzenleme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            
        }
    }
}
