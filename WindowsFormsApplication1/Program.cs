﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace rbsProje
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (System.Diagnostics.Process.GetProcessesByName("rbsProje").Length > 1)
            {
                MessageBox.Show("Program zaten açık.", "Rehberlik Servisi Kayıt İşlemleri", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new AnaEkran());
            }
        }
    }
}
