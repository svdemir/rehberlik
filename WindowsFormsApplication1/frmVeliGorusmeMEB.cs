﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace rbsProje
{
    public partial class frmVeliGorusmeMEB : Form
    {
        private static frmVeliGorusmeMEB instance;

        public static frmVeliGorusmeMEB GetForm
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new frmVeliGorusmeMEB();
                }
                else
                {
                    instance.BringToFront();
                }
                return instance;
            }
        }

        public frmVeliGorusmeMEB()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void frmUyeGorusmeMEB_Load(object sender, EventArgs e)
        {
            uCuyeListele21.btnDetayGetir.Visible = false;
            uCuyeListele21.btnResimDuzenle.Visible = false;
            uCuyeListele21.treeView1.Size = new Size(229, 220);
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DBkaynakVerileriGetir();
        }

        private void DBkaynakVerileriGetir()
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                cmbGorusmeyiYapan.DataSource = veritabanim.rehberOgretmenler.OrderBy(p => p.rehberOgretmenAdSoyad).ToList();
                cmbGorusmeyiYapan.DisplayMember = "rehberOgretmenAdSoyad";
                cmbGorusmeyiYapan.ValueMember = "rehberID";

                clistGorusmeSonuc.DataSource = veritabanim.kaynakVeliSonucCumleleri.OrderBy(s => s.sonucCumlesi).ToList();
                clistGorusmeSonuc.DisplayMember = "sonucCumlesi";
                clistGorusmeSonuc.ValueMember = "cumleID";
            }
        }

        private void btnOgrenciGorusmeYazdir_Click(object sender, EventArgs e)
        {
            if (uCuyeListele21.txtAd.Text != null && uCuyeListele21.txtAd.Text != "")
            {
                PrintDocument doc = new PrintDocument();
                doc.PrintPage += new PrintPageEventHandler(doc_PrintPage);
                PrintPreviewDialog pdg = new PrintPreviewDialog();
                pdg.Document = doc;
                pdg.Height = 700;
                pdg.Width = 900;
                pdg.Show();
            }
            else
            {
                MessageBox.Show("Alanları boş bırakmayınız.", "Yazdırma Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            Image arkaplan = Properties.Resources.veliForm;
            //e.Graphics.DrawImage(arkaplan, e.Graphics.VisibleClipBounds);
            e.Graphics.DrawImage(arkaplan, new RectangleF(0, 0, 825, 1170));

            string tarih = dateTimePicker1.Value.Date.ToString("dd MMMM yyyy");
            e.Graphics.DrawString(tarih, new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(600, 120, 620, 365));

            string ogrenciAd = uCuyeListele21.txtAd.Text;
            e.Graphics.DrawString(ogrenciAd, new System.Drawing.Font(new FontFamily("Segoe UI"), 11, FontStyle.Bold), Brushes.Black, new RectangleF(245, 205, 620, 365));

            string cinsiyet = uCuyeListele21.txtCinsiyet.Text;
            string cinsiyetKisa = cinsiyet.Substring(0, 1);
            if (cinsiyetKisa == "K" || cinsiyetKisa == "k")
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(590, 203, 620, 365));
            }
            else
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(638, 203, 620, 365));
            }
            
            string ogrenciNo = uCuyeListele21.txtSinif.Text + "        " + uCuyeListele21.txtNo.Text;
            e.Graphics.DrawString(ogrenciNo, new System.Drawing.Font(new FontFamily("Segoe UI"), 11, FontStyle.Bold), Brushes.Black, new RectangleF(600, 250, 620, 365));

            string veliAd = uCuyeListele21.txtVeliAdSoyad.Text;
            e.Graphics.DrawString(veliAd, new System.Drawing.Font(new FontFamily("Segoe UI"), 11, FontStyle.Bold), Brushes.Black, new RectangleF(245, 250, 620, 365));

            string veliYakinlik = uCuyeListele21.txtVeliYakinligi.Text;
            e.Graphics.DrawString(veliYakinlik, new System.Drawing.Font(new FontFamily("Segoe UI"), 11, FontStyle.Bold), Brushes.Black, new RectangleF(250, 298, 620, 365));

            string veliTel = uCuyeListele21.txtVeliTel.Text;
            e.Graphics.DrawString(veliTel, new System.Drawing.Font(new FontFamily("Segoe UI"), 11, FontStyle.Bold), Brushes.Black, new RectangleF(600, 298, 620, 365));



            //konu gelecek

            if (chkGorusmeKonusu1.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(245, 333, 620, 365));
            }
            if (chkGorusmeKonusu2.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(245, 365, 620, 365));
            }
            if (chkGorusmeKonusu3.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(245, 394, 620, 365));
            }
            if (chkGorusmeKonusu4.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(417, 333, 620, 365));
            }
            if (chkGorusmeKonusu5.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(417, 365, 620, 365));
            }
            if (chkGorusmeKonusu6.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(417, 394, 620, 365));
            }
            if (chkGorusmeKonusu7.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(555, 333, 620, 365));
            }
            if (chkGorusmeKonusu8.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(555, 365, 620, 365));
            }
            if (chkGorusmeKonusu9.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(555, 394, 620, 365));
            }
            if (chkGorusmeKonusu10.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(680, 394, 620, 365));
            }

            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;

            StringFormat format2 = new StringFormat();
            format2.LineAlignment = StringAlignment.Center;

            string gorusmeMetni = richTextBox1.Text;
            //e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(242, 445, 515, 106));  //görüşme metni 
            e.Graphics.DrawString(gorusmeMetni, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(245, 445, 515, 106), format2);
            e.Graphics.DrawString(tarih, new System.Drawing.Font(new FontFamily("Segoe UI"), 10, FontStyle.Bold), Brushes.Black, new RectangleF(90, 525, 620, 365));
            
            string yonKurumMetni = richTextBox2.Text;
            //e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(242, 550, 515, 106));  //yonlendirme metni 
            e.Graphics.DrawString(yonKurumMetni, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(245, 550, 515, 106), format2);


            string sonucCumlesi = "";
            for (int i = 0; i < (clistGorusmeSonuc.CheckedItems.Count); i++)
            {
                sonucCumlesi += "\t\u2022 " + ((kaynakVeliSonucCumleleri)clistGorusmeSonuc.CheckedItems[i]).sonucCumlesi + "\n";
            }
            //e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(242, 656, 515, 75));  //Sonuçlar  
            e.Graphics.DrawString(sonucCumlesi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(245, 656, 515, 75), format2);


            string gorusmeYapanKisi = cmbGorusmeyiYapan.Text;
            e.Graphics.DrawString(gorusmeYapanKisi, new System.Drawing.Font(new FontFamily("Segoe UI"), 10, FontStyle.Bold), Brushes.Black, new RectangleF(545, 742, 230, 130),format);

        }
    }
}
