﻿namespace rbsProje
{
    partial class frmVeliZiyaret
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVeliZiyaret));
            this.btnGuncelle = new System.Windows.Forms.Button();
            this.uCveliZiyaret1 = new rbsProje.Kontroller.Veli.UCveliZiyaret();
            this.SuspendLayout();
            // 
            // btnGuncelle
            // 
            this.btnGuncelle.Enabled = false;
            this.btnGuncelle.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGuncelle.Image = global::rbsProje.Properties.Resources.edit;
            this.btnGuncelle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuncelle.Location = new System.Drawing.Point(539, 259);
            this.btnGuncelle.Name = "btnGuncelle";
            this.btnGuncelle.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnGuncelle.Size = new System.Drawing.Size(101, 46);
            this.btnGuncelle.TabIndex = 15;
            this.btnGuncelle.Text = "         Güncelle";
            this.btnGuncelle.UseVisualStyleBackColor = true;
            this.btnGuncelle.Visible = false;
            this.btnGuncelle.Click += new System.EventHandler(this.btnGuncelle_Click);
            // 
            // uCveliZiyaret1
            // 
            this.uCveliZiyaret1.BackColor = System.Drawing.Color.Silver;
            this.uCveliZiyaret1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uCveliZiyaret1.Location = new System.Drawing.Point(112, 24);
            this.uCveliZiyaret1.Margin = new System.Windows.Forms.Padding(4);
            this.uCveliZiyaret1.Name = "uCveliZiyaret1";
            this.uCveliZiyaret1.Size = new System.Drawing.Size(744, 749);
            this.uCveliZiyaret1.TabIndex = 16;
            // 
            // frmVeliZiyaret
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(974, 798);
            this.Controls.Add(this.btnGuncelle);
            this.Controls.Add(this.uCveliZiyaret1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmVeliZiyaret";
            this.Text = "Veli Ziyareti";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.SizeChanged += new System.EventHandler(this.frmVeliZiyaret_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button btnGuncelle;
        private Kontroller.Veli.UCveliZiyaret uCveliZiyaret1;
    }
}