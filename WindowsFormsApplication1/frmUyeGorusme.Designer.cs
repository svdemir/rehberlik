﻿namespace rbsProje
{
    partial class frmUyeGorusme
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUyeGorusme));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnGuncelle = new System.Windows.Forms.Button();
            this.uCuyeGorusme21 = new rbsProje.Kontroller.Uyeler.UCuyeGorusme2();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnGuncelle
            // 
            this.btnGuncelle.Enabled = false;
            this.btnGuncelle.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGuncelle.Image = global::rbsProje.Properties.Resources.edit;
            this.btnGuncelle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuncelle.Location = new System.Drawing.Point(649, 407);
            this.btnGuncelle.Name = "btnGuncelle";
            this.btnGuncelle.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnGuncelle.Size = new System.Drawing.Size(101, 40);
            this.btnGuncelle.TabIndex = 13;
            this.btnGuncelle.Text = "         Güncelle";
            this.btnGuncelle.UseVisualStyleBackColor = true;
            this.btnGuncelle.Visible = false;
            this.btnGuncelle.Click += new System.EventHandler(this.btnGuncelle_Click);
            // 
            // uCuyeGorusme21
            // 
            this.uCuyeGorusme21.AutoSize = true;
            this.uCuyeGorusme21.Location = new System.Drawing.Point(13, 4);
            this.uCuyeGorusme21.Name = "uCuyeGorusme21";
            this.uCuyeGorusme21.Size = new System.Drawing.Size(1063, 923);
            this.uCuyeGorusme21.TabIndex = 0;
            // 
            // frmUyeGorusme
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1099, 781);
            this.Controls.Add(this.btnGuncelle);
            this.Controls.Add(this.uCuyeGorusme21);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmUyeGorusme";
            this.Text = "Öğrenci Görüşme";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.SizeChanged += new System.EventHandler(this.frmUyeGorusme_SizeChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Kontroller.Uyeler.UCuyeGorusme2 uCuyeGorusme21;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.Button btnGuncelle;
    }
}