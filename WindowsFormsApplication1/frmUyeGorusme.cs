﻿using System;
using System.Windows.Forms;
using System.Linq;
using System.Globalization;

namespace rbsProje
{
    public partial class frmUyeGorusme : Form
    {

        private static frmUyeGorusme instance;

        public static frmUyeGorusme GetForm
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new frmUyeGorusme();
                }
                else
                {
                    instance.BringToFront();
                }
                return instance;
            }
        }

        public frmUyeGorusme()
        {
            InitializeComponent();
        }

        long globalGorusmeID;
        bool guncellemeIslemi = false;

        public frmUyeGorusme(long gorusmeID, bool guncelleme)
        {
            InitializeComponent();
            uCuyeGorusme21.Enabled = false;
            btnGuncelle.Visible = true;
            globalGorusmeID = gorusmeID;
            guncellemeIslemi = guncelleme;
        }

        private void VerileriGetir(long gorusmeID)
        {
            try
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var guncellenecekGorusmeObje = veritabanim.gorusmeOgrenci.Where(g => g.gorusmeID == gorusmeID).SingleOrDefault();
                    long geciciNo = guncellenecekGorusmeObje.ogrNo.Value;
                    uCuyeGorusme21.TemelOgrenciBilgileriniDoldur(geciciNo);
                    uCuyeGorusme21.DetayliOgrenciBilgileriniDoldur(geciciNo);
                    ((ComboBox)uCuyeGorusme21.Controls["groupBox3"].Controls["cmbGorusmeyiYapan"]).SelectedValue = guncellenecekGorusmeObje.calismayiYapan;
                    ((ComboBox)uCuyeGorusme21.Controls["groupBox3"].Controls["cmbGorusmeTuru"]).SelectedValue = guncellenecekGorusmeObje.gorusmeTuru;
                    ((ComboBox)uCuyeGorusme21.Controls["groupBox3"].Controls["cmbPdralanlari"]).SelectedValue = guncellenecekGorusmeObje.pdrAlani;

                    string format = "yyyyMMdd";
                    DateTime dt;
                    var z = DateTime.TryParseExact(guncellenecekGorusmeObje.gorusmeTarihi, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                    ((DateTimePicker)uCuyeGorusme21.Controls["groupBox3"].Controls["dateTimePicker1"]).Value = dt;


                    var guncellenecekKonuObje = veritabanim.analizGorusmeKonulari.Where(g => g.gorusmeID == gorusmeID).SingleOrDefault();
                    ((RichTextBox)uCuyeGorusme21.Controls["groupBox3"].Controls["richTextBox1"]).Text = guncellenecekKonuObje.gorusmeKonusu;


                    var guncellenecekYapCalObje = veritabanim.analizYapilacakCalismalar.Where(g => g.gorusmeID == gorusmeID).ToList();
                    if (guncellenecekYapCalObje.Count > 0)
                    {
                        for (int i = 0; i < ((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).Items.Count; i++)
                        {
                            for (int j = 0; j < guncellenecekYapCalObje.Count; j++)
                            {
                                if (((kaynakYapilacakCalismalar)((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).Items[i]).yapilacakID == Convert.ToInt64(guncellenecekYapCalObje[j].yapilacakCalisma))
                                {
                                    ((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).SetItemChecked(i, true);
                                    ((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).SetSelected(i, true);
                                }
                            }
                        }
                    }



                    var guncellenecekSorunObje = veritabanim.analizSorunAlanlari.Where(g => g.gorusmeID == gorusmeID).ToList();
                    if (guncellenecekSorunObje.Count > 0)
                    {
                        for (int i = 0; i < ((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistSorunAlanlari"]).Items.Count; i++)
                        {
                            for (int j = 0; j < guncellenecekSorunObje.Count; j++)
                            {
                                if (((kaynakSorunAlanlari)((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistSorunAlanlari"]).Items[i]).alanID == Convert.ToInt64(guncellenecekSorunObje[j].sorunAlani))
                                {
                                    ((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistSorunAlanlari"]).SetItemChecked(i, true);
                                    ((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistSorunAlanlari"]).SetSelected(i, true);
                                }
                            }
                        }
                    }

                    var guncellenecekSonucObje = veritabanim.analizSonucCumleleri.Where(g => g.gorusmeID == gorusmeID).ToList();
                    if (guncellenecekSonucObje.Count > 0)
                    {
                        for (int i = 0; i < ((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistGorusmeSonuc"]).Items.Count; i++)
                        {
                            for (int j = 0; j < guncellenecekSonucObje.Count; j++)
                            {
                                if (((kaynakSonucCumleleri)((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistGorusmeSonuc"]).Items[i]).cumleID == Convert.ToInt64(guncellenecekSonucObje[j].sonucCumlesi))
                                {
                                    ((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistGorusmeSonuc"]).SetItemChecked(i, true);
                                    ((CheckedListBox)uCuyeGorusme21.Controls["groupBox3"].Controls["clistGorusmeSonuc"]).SetSelected(i, true);
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception )
            {
            }

        }

        private void frmUyeGorusme_SizeChanged(object sender, EventArgs e)
        {
            if (this.Controls.Count > 0)
            {
                btnGuncelle.Left = this.Controls[1].Left + 635;
                this.Controls[1].Left = (this.Width - this.Controls[1].Width) / 2;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (guncellemeIslemi == true)
            {
                if (((ComboBox)uCuyeGorusme21.Controls["groupBox3"].Controls["cmbGorusmeyiYapan"]).Items.Count > 0)
                {
                    VerileriGetir(globalGorusmeID);
                    uCuyeGorusme21.Enabled = true;
                    btnGuncelle.Enabled = true;
                    uCuyeGorusme21.btnAra.Enabled = false;
                    uCuyeGorusme21.btnGorusmeKaydet.Enabled = false;
                    uCuyeGorusme21.btnTumunuGetir.Enabled = false;
                    uCuyeGorusme21.textBox1.Enabled = false;
                    timer1.Enabled = false;
                }
            }
            else
            {
                timer1.Enabled = false;
            }
        }

        private void uCuyeGorusme21_Load(object sender, EventArgs e)
        {

        }

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            GorusmeSil(globalGorusmeID);
            uCuyeGorusme21.GorusmeKaydetme(globalGorusmeID.ToString());
        }

        private void GorusmeSil(long gorusmeID)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObje = veritabanim.gorusmeOgrenci.Where(s => s.gorusmeID == gorusmeID).SingleOrDefault();
                veritabanim.gorusmeOgrenci.Remove(silinecekObje);

                var silinecek1 = veritabanim.analizGorusmeKonulari.Where(s => s.gorusmeID == gorusmeID).SingleOrDefault();
                veritabanim.analizGorusmeKonulari.Remove(silinecek1);

                var silinecek2 = veritabanim.analizSonucCumleleri.Where(s => s.gorusmeID == gorusmeID).ToList();
                veritabanim.analizSonucCumleleri.RemoveRange(silinecek2);

                var silinecek3 = veritabanim.analizSorunAlanlari.Where(s => s.gorusmeID == gorusmeID).ToList();
                veritabanim.analizSorunAlanlari.RemoveRange(silinecek3);

                var silinecek4 = veritabanim.analizYapilacakCalismalar.Where(s => s.gorusmeID == gorusmeID).ToList();
                veritabanim.analizYapilacakCalismalar.RemoveRange(silinecek4);

                try
                {
                    veritabanim.SaveChanges();

                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

    }
}
